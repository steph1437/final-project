![alt](https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)

---


# IN A FLASH
> In A Flash is a web application enabling registered users to be able to participate in competitions related to the respective category, to upload photos which are subsequently evaluated by a jury, can accumulate points and raise the rank in the respective ranking.


> The data of the application is stored in a relational database. The application consumes one public REST services in order to achieve its main functionality - Imgur Service. Initial Seed and subsequent Sync of database is performed only via Admin profile.
> The uploaded photos in the last shot are kept only as a URL in the database, and the main photo is stored on the site of Imgur.com

![alt](https://i.ibb.co/9qqXTm5/Diagrama.png)


---

## Technologies


- C#
- JWT
- Postman
- Swagger
- GitLab
- HTML
- CSS
- Bootstrap
- JavaScript

- Run the project:
> click on run icon for run the project.

- Database
> updates the database to the last migration

> If you want to have full functionality over the admin account, you need to log in with the following data:
> 1. Login as Admin:
>  - Username: AADMINOV@MAIL.BG
>  - Password: Parola
>
>  
>
> 2. Now you are admin, congratulations!

## Features
#### On the top you can see a menu button, which lists any of the following pages: Dashboard, Login, Register, Create Contest, Top Rated.


#### On the home page you can see top rated pictures. If contest is already in phase two, you cannot upload file, but jury can rate them.

#### If you are a registered user, in your account you have information about all the contests that are active.

#### You can also take part in one of the open contests and upload a photo (i.e. to participate in the contest)

#### If the competition is in phase 2, the jury can review photos in the competition and write comments. 

## Documentation

#### API documentation is accessed only through authentication. Two options are available - via Identity or via JSON Web Token. Admin role is required to access Adm API Controller.

## Test coverage


#### Up to 82% test code coverage.


## Team
* Martin Mihaylov - [GitLab](https://gitlab.com/MartinMIhaylov77)
* Stefan Zhuntovski - [GitLab](https://gitlab.com/steph1437)

---
#
||[Trello](https://trello.com/b/tQekI1Bb/team-twelve-in-a-flash)|[Swagger](https://localhost:5001/swagger/index.html)
|:------------:|:------------:|:------------:|
