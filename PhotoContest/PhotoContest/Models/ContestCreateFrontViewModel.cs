﻿using Microsoft.AspNetCore.Http;
using PhotoContest.Services.Providers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Models
{
    public class ContestCreateFrontViewModel
    {
        public int Id { get; set; }
        //[Required(ErrorMessage = "*Required")]
        //[StringLength(maximumLength: 20)]
        //[Display(Name = "Name")]
        public string Title { get; set; }


        [Display(Name = "Cover Url")]
        public string Foreground_Url { get; set; }

        //[Required(ErrorMessage = "*Required")]
        //[StringLength(maximumLength: 250)]
        ////[Display(Name = "Decription")]
        public string ContestCategoryPhaseDescription { get; set; }

        public PhaseProvider Phases { get; set; }

        [Required(ErrorMessage = "*Required")]
        [Display(Name = "Contest Category")]
        public int ContestCategoryId { get; set; }

        //[Required(ErrorMessage = "*Required")]
        //[Display(Name = "Contest Type")]
        public int ContestStateId { get; set; }
    }
}
