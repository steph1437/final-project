﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Models
{
    public class CreateReviewModel
    {
        public int PhotoId { get; set; }

        public bool DoesntFitsCategoryCheckBox { get; set; }

        public string Comment { get; set; }

        public double? Score { get; set; }
    }
}
