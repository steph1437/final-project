﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Models
{
    public class ContestFrontViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ContestCategoryPhaseDescription { get; set; }
        public ContestCategoryViewModel ContestCategory { get; set; }
        public ICollection<PictureFrontViewModel> Pictures { get; set; }
        public string ContestStateName { get; set; }
        public string Foreground_Url { get; set; }
        public DateTime PhaseStartTime { get; set; }
        public DateTime PhaseEndTime { get; set; }
    }
}
