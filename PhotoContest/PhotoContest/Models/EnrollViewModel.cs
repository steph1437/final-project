﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Models
{
    public class EnrollViewModel
    {
        public int UserId { get; set; }


        public int ContestId { get; set; }

        public int Id { get; set; }

        [Required]
        [StringLength(15, MinimumLength = 3)]
        [Display(Name = "Picture Title")]
        public string Name { get; set; }


        [Required]
        [StringLength(200, MinimumLength = 3)]
        [Display(Name = "Picture Story")]
        public string Story { get; set; }

        [Required]
        public string Url { get; set; }
    }
}
