﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Models
{
    public class PictureReviewIFrontViewModel
    {

        public int Id { get; set; }

        public int PictureId { get; set; }

        //public  JuryContestViewModel  { get; set; }

        public bool DoesntFitsCategoryCheckBox { get; set; }

        public string Comment { get; set; }

        public double? Score { get; set; }
    }
}
