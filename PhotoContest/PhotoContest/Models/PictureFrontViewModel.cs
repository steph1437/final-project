﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Models
{
    public class PictureFrontViewModel
    {
        public int Id { get; set; }


        public string Name { get; set; }


        public string Story { get; set; }


        public string Url { get; set; }

        public ContestViewModel Contest { get; set; }

        public ICollection<PictureReviewIFrontViewModel> PictureReviews { get; set; }

       

        public bool IsReviewable { get; set; }

        //public ContenderContestDTO ContenderContest { get; set; }
    }
}
