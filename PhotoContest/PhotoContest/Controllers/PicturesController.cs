﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Data;
using PhotoContest.Models;
using PhotoContest.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Controllers
{
    public class PicturesController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly IPictureService pictureService;
        private readonly IMapper mapper;
        public PicturesController(IPictureService pictureService, IMapper mapper, UserManager<User> userManager)
        {
            this.pictureService = pictureService ?? throw new ArgumentNullException(nameof(pictureService));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }
        public async Task<IActionResult> Details(int id)
        {
            var pictureDTO = await this.pictureService.GetPictureAsync(id);

            if (pictureDTO == null)
            {
                return NotFound();
            }

            var pictureModel = mapper.Map<PictureFrontViewModel>(pictureDTO);
            double avScore = 0;

            foreach (var item in pictureDTO.PictureReviews)
            {
                avScore += item.Score;
            }

            pictureModel.PictureReviews.Select(x => x.Score).Sum().ToString().Where(x => x.Equals(avScore / pictureDTO.PictureReviews.Count()));

            return View(pictureModel);
        }

        

    }
}
