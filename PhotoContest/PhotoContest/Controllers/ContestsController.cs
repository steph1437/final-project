﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Models;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using PhotoContest.Data;

namespace PhotoContest.Controllers
{
    public class ContestsController : Controller
    {
        private readonly IMapper mapper;
        private readonly IContestService contestService;
        private readonly IPhaseService phaseService;
        private readonly IContestStateService contestStateService;
        private readonly IContestCategoryService contestCategoryService;
        private readonly UserManager<User> userManager;

        public ContestsController(UserManager<User> userManager, IContestService contestService, IPhaseService phaseService, IContestStateService contestStateService, IContestCategoryService contestCategoryService, IMapper mapper)
        {

            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.contestService = contestService;
            this.phaseService = phaseService;
            this.contestStateService = contestStateService;
            this.contestCategoryService = contestCategoryService;
            this.userManager = userManager;

        }
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            ViewBag.Categories = await this.contestCategoryService
                    .GetAllContestCategoriesAsync();

            ViewBag.Phases = await this.phaseService
                .GetAllContestPhasesAsync();

            ViewBag.ContestTypes = await this.contestStateService
                .GetAllContestStatesAsync();

            return View();
        }

        public async Task<IActionResult> Details(int id)
        {
            var contestDTO = await this.contestService.GetContestAsync(id);
            if (contestDTO == null)
            {
                return NotFound();
            }

    

            var userId = userManager.GetUserId(User);

            var user = await userManager.GetUserAsync(User);


            foreach (var picture in contestDTO.Pictures)
            {
                picture.IsReviewable = true;

                foreach (var review in picture.PictureReviews)
                {
                    if (review.JuryContestContestId.ToString().Equals(userId))
                    {
                        picture.IsReviewable = false;
                    }
                }
            }

            var contestModel = mapper.Map<ContestFrontViewModel>(contestDTO);




            contestModel.PhaseStartTime = contestDTO.Phase.StartDate;
            contestModel.PhaseEndTime = contestDTO.Phase.EndDate;
            return View(contestModel);
        }

        [HttpPost]
        public async Task<IActionResult> Create(ContestCreateFrontViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    model.ContestCategoryPhaseDescription = "Phase One";
                    var contestDTO = mapper.Map<ContestInputDTO>(model);
                    ContestOutputFrontDTO newContest = await this.contestService.CreateAsync(contestDTO);
                    var beerModel = mapper.Map<ContestCreateFrontViewModel>(newContest);

                    return RedirectToAction("Index", "Dashboard");
                }
                catch (ResourseAlreadyExistException)
                {
                    ModelState.AddModelError("Name", "Contest with that name already exist in this application!");
                }
                catch (InvalidParameterException e)
                {
                    ModelState.AddModelError(String.Empty, e.Message);
                }
            }

            return View(model);
        }

        public async Task<IActionResult> Users(int id)
        {
            var userDTO = await this.contestService.SeeContestUsers(id);
            List<UserViewModel> mappedUsers = new List<UserViewModel>();
            foreach(var user in userDTO)
            {
                mappedUsers.Add(mapper.Map<UserViewModel>(user));
            }
            return View(mappedUsers);
        }
    }
}