﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using PhotoContest.Models;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Controllers
{
    public class ReviewController : Controller
    {
        public IPictureService pictureService;
        public IReviewService reviewService;
        public IMapper mapper;
        private readonly IToastNotification toastNotification;

        public ReviewController(IReviewService reviewService, IMapper mapper, IPictureService pictureService, IToastNotification toastNotification)
        {
            this.reviewService = reviewService ?? throw new ArgumentNullException(nameof(reviewService));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.pictureService = pictureService ?? throw new ArgumentNullException(nameof(pictureService));
            this.toastNotification = toastNotification ?? throw new ArgumentNullException(nameof(toastNotification));

            //this.userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateReview(PictureReviewIFrontViewModel model)
        {
            if (true)//Current user може да прави ревю
            {

            }
            if (ModelState.IsValid)
            {
                try
                {

                    var reviewDTO = mapper.Map<PictureReviewDTO>(model);
                    await this.reviewService.CreatePictureReviewAsync(reviewDTO);
                    this.toastNotification.AddSuccessToastMessage("The review is posted");

                    return RedirectToAction("Details", "Pictures", new { id = model.PictureId });
                }
                catch (InvalidParameterException)
                {
                    this.toastNotification.AddErrorToastMessage("The comment must be between 3 and 500 symbols");
                }

            }
            return RedirectToAction("Details", "Pictures", new { id = model.PictureId });
        }
    }
 
}