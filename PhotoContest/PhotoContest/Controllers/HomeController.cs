﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PhotoContest.Models;
using PhotoContest.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMapper mapper;
        private readonly IHttpContextAccessor httpContext;
        private readonly IPictureService pictureService;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, IPictureService pictureService, IMapper mapper, IHttpContextAccessor httpContext)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.httpContext = httpContext;
            this.pictureService = pictureService ?? throw new ArgumentNullException(nameof(pictureService));
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            var username = HttpContext.User.Identity.IsAuthenticated;

            var topThreePictureDTO = await this.pictureService.GetTopThreeRatedPictureAsync();

            if (topThreePictureDTO == null)
            {
                return NotFound();
            }

            var bestRatedPicture = mapper.Map<ICollection<PictureFrontViewModel>>(topThreePictureDTO);
            return View(bestRatedPicture);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}