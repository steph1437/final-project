﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using PhotoContest.Data;
using PhotoContest.Models;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Controllers
{
    public class DashboardController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly IContestService contestService;
        private readonly IJunkieService junkieService;
        private readonly IMapper mapper;
        public DashboardController(IContestService contestService, IMapper mapper, UserManager<User> userManager, IJunkieService junkieService)
        {
            this.userManager = userManager;
            this.mapper = mapper;
            this.contestService = contestService;
            this.junkieService = junkieService;
        }
        public async Task<IActionResult> Index(string search)
        {
            ViewBag.Search = search;

            var allContestDTO = await this.contestService.ListAllFilterHybrid(search);

            if (allContestDTO == null)
            {
                return NotFound();
            }

            var contests = mapper.Map<ICollection<ContestFrontViewModel>>(allContestDTO).ToList();

            return View(contests);
        }
        public IActionResult Enroll(int contestId)
        {

            return PartialView("~/Views/Dashboard/Enroll.cshtml",
                new EnrollViewModel() { ContestId = contestId });
        }

        [HttpPost]
        public async Task<IActionResult> Enroll(EnrollViewModel model)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Dashboard", new { search = "" });
            }

            var userId = userManager.GetUserId(User);

            // Cannot enroll if user is jury for contest
            if (await this.junkieService.IsUserJury(model.ContestId, int.Parse(userId)))
            {
                return RedirectToAction("Index", "Dashboard", new { search = "" });
            }

            var enrollDTO = mapper.Map<EnrollContestDTO>(model);
            await this.junkieService.EnrollForContestAsync(enrollDTO);

            return RedirectToAction("Details", "Contest", new { id = model.ContestId});
        }

    }
}
