﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Data;
using PhotoContest.Models;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.ApiControllers
{
    [Route("api/contests")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
    public class ContestApiController : ControllerBase
    {
        private readonly IContestService contestService;
        private readonly IMapper mapper;
        private readonly UserManager<User> userManager;

        public ContestApiController(IContestService contestService, IMapper mapper, UserManager<User> userManager)
        {
            this.contestService = contestService ?? throw new ArgumentNullException(nameof(contestService));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.userManager = userManager;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetContest(int id)
        {
            var contestDTO = await contestService.GetContestAsync(id);

            if (contestDTO == null)
            {
                return this.NotFound();
            }
            else
            {
                var contestView = mapper.Map<ContestViewModel>(contestDTO);
                return this.Ok(contestView);
            }
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> ListContests()
        {
            var models = contestService.GetAllContestsAsync()
                  .Result.Select(dto => mapper.Map<ContestInputViewModel>(dto));

            return this.Ok(models);
        }

        [HttpPost]
        [Route("")] 
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> CreateContest([FromBody] ContestInputViewModel createData)
        {


            var contestDTO = mapper.Map<ContestDTO>(createData);

            try
            {
                var newContest = await contestService.CreateContestAsync(contestDTO);
                var contestView = mapper.Map<ContestInputViewModel>(contestDTO);
                return this.Created("CreateContest", contestView);
            }
            catch (InvalidParameterException)
            {
                return this.BadRequest();
            }
        }

        [HttpPut]
        [Route("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateContest([FromBody] ContestInputViewModel updateData, int id)
        {
            if (!ModelState.IsValid)
            {
                this.BadRequest();
            }

            try
            {
                var contestDTO = mapper.Map<ContestDTO>(updateData);
                contestDTO.Id = id;
                await  contestService.UpdateContestAsync(contestDTO);
                return this.NoContent();
            }
            catch (ResourceNotExistException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (InvalidParameterException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (ResourseAlreadyExistException e)
            {
                return this.BadRequest(e.Message);
            }
        }


        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
               await  contestService.DeleteContestAsync(id);
                return this.NoContent();
            }
            catch (ResourceNotExistException)
            {
                return this.NotFound();
            }
        }
    }
}
