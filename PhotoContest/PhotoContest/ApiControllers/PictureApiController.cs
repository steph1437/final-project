﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Models;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.ApiControllers
{
    [Route("api/pictures")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
    public class PictureApiController : ControllerBase
    {
        private readonly IPictureService pictureService;
        private readonly IMapper mapper;

        public PictureApiController(IPictureService pictureService, IMapper mapper)
        {
            this.pictureService = pictureService ?? throw new ArgumentNullException(nameof(pictureService));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetPicture(int id)
        {
            var pictureReviewDTO = await pictureService.GetPictureAsync(id);

            if (pictureReviewDTO == null)
            {
                return this.NotFound();
            }
            else
            {
                var pictureView = mapper.Map<PictureViewModel>(pictureReviewDTO);
                return this.Ok(pictureView);
            }
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> ListPictureReviews()
        {
            var models = pictureService
                .GetAllPicturesAsync()
                .Result.Select(dto => mapper.Map<PictureViewModel>(dto));

            return this.Ok(models);
        }

        [HttpPost]
        [Route("")]
        [Authorize(Roles = "user")]
        public async Task<IActionResult> CreatePicture([FromBody] PictureViewModel createData)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest();
            }

            var pictureDTO = mapper.Map<PictureDTO>(createData);

            try
            {
                var newPicture = await pictureService.CreatePictureAsync(pictureDTO);
                var pictureView = mapper.Map<PictureViewModel>(pictureDTO);
                return this.Created("CreatePictureReview", pictureView);
            }
            catch (InvalidParameterException)
            {
                return this.BadRequest();
            }
        }

        [HttpPut]
        [Route("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdatePicture([FromBody] PictureInputViewModel updateData, int id)
        {
            if (!ModelState.IsValid)
            {
                this.BadRequest();
            }

            try
            {
                var pictureDTO = mapper.Map<PictureDTO>(updateData);
                pictureDTO.Id = id;
                await pictureService.UpdatePictureAsync(pictureDTO);
                return this.NoContent();
            }
            catch (ResourceNotExistException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (InvalidParameterException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (ResourseAlreadyExistException e)
            {
                return this.BadRequest(e.Message);
            }
        }


        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
              await pictureService.DeletePictureAsync(id);
                return this.NoContent();
            }
            catch (ResourceNotExistException)
            {
                return this.NotFound();
            }
        }
    }
}
