﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Models;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.ApiControllers
{
    [Route("api/categories")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
    public class ContestCategoryApiController : ControllerBase
    {
        private readonly IContestCategoryService contestCategoryService;
        private readonly IMapper mapper;

        public ContestCategoryApiController(IContestCategoryService contestCategoryService, IMapper mapper)
        {
            this.contestCategoryService = contestCategoryService ?? throw new ArgumentNullException(nameof(contestCategoryService));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet("{id}")]
        public async Task< IActionResult> GetCategory(int id)
        {
            var categoryDTO = await contestCategoryService.GetContestCategoryAsync(id);

            if (categoryDTO == null)
            {
                return NotFound();
            }
            else
            {
                var categoryViewModel = mapper.Map<ContestCategoryViewModel>(categoryDTO);
                return Ok(categoryViewModel);
            }
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> ListContestCategories()
        {
            var models =  contestCategoryService.GetAllContestCategoriesAsync()
                                               .Result
                                               .Select(dto => mapper.Map<ContestCategoryViewModel>(dto));

            return Ok(models);
        }

        [HttpPost]
        [Route("")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> CreateCategory([FromBody] ContestCategoryViewModel contestCategoryModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var categoryDTO = mapper.Map<ContestCategoryDTO>(contestCategoryModel);

            try
            {
                var newCtageory = await contestCategoryService.CreateContestCategoryAsync(categoryDTO);

                var contestCategoryViewModel = mapper.Map<ContestCategoryViewModel>(newCtageory);
                return Created("CreateCategory", contestCategoryViewModel);
            }
            catch (Exception)
            {

                throw;
            }


        }

        [HttpPut]
        [Route("{id}")]
        [Authorize(Roles = "admin")]
        public async Task< IActionResult> UpdateCategory([FromBody] ContestCategoryViewModel contestCategoryViewModel, int id)
        {
            if (!ModelState.IsValid)
            {
                BadRequest();
            }

            try
            {
                var categoryDTO = mapper.Map<ContestCategoryDTO>(contestCategoryViewModel);
                categoryDTO.Id = id;

               await contestCategoryService.UpdateContestCategoryAsync(categoryDTO);

                return NoContent();
            }
            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }
            catch (ResourseAlreadyExistException e)
            {
                return BadRequest(e.Message);
            }
            catch (ResourceNotExistException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public async Task< IActionResult> Delete(int id)
        {
            try
            {
               await contestCategoryService.DeleteContestCategoryAsync(id);
                return NoContent();
            }
            catch (ResourceNotExistException)
            {
                return NotFound();
            }
            catch (ResourseInUseException)
            {
                return Conflict();
            }
        }
    }
}

