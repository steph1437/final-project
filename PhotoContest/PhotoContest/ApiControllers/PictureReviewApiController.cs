﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Models;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.ApiControllers
{
    [Route("api/reviews")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
    public class PictureReviewApiController : ControllerBase
    {
        private readonly IReviewService pictureReviewService;
        private readonly IMapper mapper;

        public PictureReviewApiController(IReviewService pictureReviewService, IMapper mapper)
        {
            this.pictureReviewService = pictureReviewService ?? throw new ArgumentNullException(nameof(pictureReviewService));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetPictureReview(int id)
        {
            var pictureReviewDTO = await pictureReviewService.GetPictureReviewAsync(id);

            if (pictureReviewDTO == null)
            {
                return this.NotFound();
            }
            else
            {
                var pictoreReviewView = mapper.Map<PictureReviewViewModel>(pictureReviewDTO);
                return this.Ok(pictoreReviewView);
            }
        }

        [HttpGet]
        [Route("")]
        public async Task< IActionResult> ListReviews()
        {
            var models =   pictureReviewService .GetAllPictureReviewsAsync()
                                                .Result.Select(dto => mapper.Map<PictureReviewViewModel>(dto));

            return Ok(models);
        }

        [HttpPost]
        [Route("")]
        [Authorize(Roles = "admin")]
        public async Task< IActionResult> CreatePictureReview([FromBody] PictureReviewViewModel createData)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest();
            }

            var pictureReviewDTO = mapper.Map<PictureReviewDTO>(createData);

            try
            {
                var newPictureReview = await pictureReviewService.CreatePictureReviewAsync(pictureReviewDTO);
                var pictureReviewView = mapper.Map<PictureReviewViewModel>(pictureReviewDTO);
                return this.Created("CreatePictureReview", pictureReviewView);
            }
            catch (InvalidParameterException)
            {
                return this.BadRequest();
            }
        }

        [HttpPut]
        [Route("{id}")]
        [Authorize(Roles = "admin")]
        public async Task< IActionResult> UpdatePictureReview([FromBody] PictureReviewViewModel updateData, int id)
        {
            if (!ModelState.IsValid)
            {
                this.BadRequest();
            }

            try
            {
                var pictureReviewDTO = mapper.Map<PictureReviewDTO>(updateData);
                pictureReviewDTO.Id = id;
                await pictureReviewService.UpdatePictureReviewAsync(pictureReviewDTO);
                return this.NoContent();
            }
            catch (ResourceNotExistException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (InvalidParameterException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (ResourseAlreadyExistException e)
            {
                return this.BadRequest(e.Message);
            }
        }


        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
               await pictureReviewService.DeletePictureReviewAsync(id);
                return this.NoContent();
            }
            catch (ResourceNotExistException)
            {
                return this.NotFound();
            }
        }
    }
}

