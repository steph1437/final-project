﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Models;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.ApiControllers
{
    [Route("api/users")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
    public class UserApiController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly IMapper mapper;
        public UserApiController(IUserService userService, IMapper mapper)
        {
            this.userService = userService ?? throw new ArgumentNullException(nameof(userService));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet]
        [Route("{id}")]
        public async Task <IActionResult> GetUser(int id)
        {
            var userDTO = await userService.GetUserAsync(id);

            if (userDTO == null)
            {
                return NotFound();
            }
            else
            {
                var userModel = mapper.Map<UserViewModel>(userDTO);

                return Ok(userModel);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAllUsers()
        {
            var userDTO = await userService.GetAllUsersAsync();

            if (userDTO == null)
            {
                return NotFound();
            }
            else
            {
                var userModel = mapper.Map<List<UserViewModel>>(userDTO);
                return Ok(userModel);
            }
        }

        [HttpPost]
        [Route("")]
        [AllowAnonymous]
        public async Task<IActionResult> CreateUser([FromBody] UserViewModel userviewModel)
        {
            try
            {
                var userDTO = await userService.CreateUserAsync(mapper.Map<UserDTO>(userviewModel));

                if (userDTO == null)
                {
                    return NotFound();
                }
                else
                {
                    var userModel = mapper.Map<UserViewModel>(userDTO);

                    return Created("CreateUser", userModel);
                }
            }
            catch (ResourseAlreadyExistException e)
            {
                return BadRequest(e.Message);
            }
            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("update/{id}/")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateUser([FromBody] UserInputViewModel userViewModel, int id)
        {
            if (!ModelState.IsValid)
            {
                BadRequest();
            }

            try
            {
                var userDTO = mapper.Map<UserDTO>(userViewModel);
                userDTO.Id = id;

                await userService.UpdateUserAsync(userDTO);
                return NoContent();
            }
            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }
            catch (ResourseAlreadyExistException e)
            {
                return BadRequest(e.Message);
            }
            catch (ResourceNotExistException e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpDelete]
        [Route("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteUser([FromRoute] int id)
        {
            try
            {
                await userService.DeleteUserAsync(id);

                return Ok();
            }
            catch (ResourceNotExistException e)
            {
                return BadRequest(e.Message);
            }
            catch (ResourseAlreadyExistException e)
            {
                return BadRequest(e.Message);
            }
            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }
        }

       /* [HttpPost("authenticate")]
        [AllowAnonymous]
        public IActionResult Authenticate(string username, string password)
        {
            var user = userService.Authenticate(username, password);

            if (user.Item1 == null)
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }
            return Ok(user.Item2);
        }*/
    }
}
