﻿using PhotoContest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Models
{
    public class ContestViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public string ContestCategoryPhaseDescription { get; set; }
        public string ContestCategoryName { get; set; }
        public string ContestStateName { get; set; }
        public string Foreground_Url { get; set; }
        public DateTime CreatedOn { get; set; }

    
    }
}
