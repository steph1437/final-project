﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Models
{
    public class ContestInputViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ContestCategoryPhaseDescription { get; set; }

        public int ContestStateId { get; set; }
    }
}
