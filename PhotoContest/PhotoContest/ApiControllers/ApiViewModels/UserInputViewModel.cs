﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Models
{
    public class UserInputViewModel
    {
        public string Email { get; set; }

        public string UserName { get; set; }
    }
}
