﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Models
{
    public class PictureInputViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Story { get; set; }

        public string Url { get; set; }


      
    }
}
