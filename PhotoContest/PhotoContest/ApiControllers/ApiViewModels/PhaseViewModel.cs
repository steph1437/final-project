﻿using PhotoContest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Models
{
    public class PhaseViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

    }
}
