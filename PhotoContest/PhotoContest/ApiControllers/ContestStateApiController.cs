﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Models;
using PhotoContest.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.ApiControllers
{
    [Route("api/states")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
    public class ContestStateApiController : ControllerBase
    {
      
        private readonly IContestStateService contestStateService;
        private readonly IMapper mapper;
        public ContestStateApiController(IContestStateService contestStateService, IMapper mapper)
        {
            this.contestStateService = contestStateService ?? throw new ArgumentNullException((nameof(contestStateService)));
            this.mapper = mapper ?? throw new ArgumentNullException((nameof(mapper)));
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> ListContestStates()
        {
            var models =  contestStateService.GetAllContestStatesAsync()
                                            .Result
                                            .Select(dto => mapper.Map<ContestStateViewModel>(dto));

            return Ok(models);
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetState(int id)
        {
            var stateDTO = await contestStateService.GetContestStateAsync(id);

            if (stateDTO == null)
            {
                return NotFound();
            }
            else
            {
                var contestStateViewModel = mapper.Map<ContestStateViewModel>(stateDTO);
                return Ok(contestStateViewModel);
            }
        }
    }
}
