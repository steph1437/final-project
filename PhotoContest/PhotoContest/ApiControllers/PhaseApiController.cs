﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Models;
using PhotoContest.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.ApiControllers
{
    [Route("api/phases")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Identity.Application," + JwtBearerDefaults.AuthenticationScheme)]
    public class PhaseApiController : ControllerBase
    {
        private readonly IPhaseService phaseService;
        private readonly IMapper mapper;
        public PhaseApiController(IPhaseService contestPhaseService, IMapper mapper)
        {
            this.phaseService = contestPhaseService ?? throw new ArgumentNullException((nameof(contestPhaseService)));
            this.mapper = mapper ?? throw new ArgumentNullException((nameof(mapper)));
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> ListPhases()
        {
            var models = phaseService.GetAllContestPhasesAsync()
                                            .Result
                                            .Select(dto => mapper.Map<PhaseViewModel>(dto));

            return Ok(models);
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPhase(int id)
        {
            var contestDTO = await phaseService.GetContestPhaseAsync(id);

            if (contestDTO == null)
            {
                return NotFound();
            }
            else
            {
                var contestPhaseViewModel = mapper.Map<PhaseViewModel>(contestDTO);
                return Ok(contestPhaseViewModel);
            }
        }
    }
}
