﻿using AutoMapper;
using PhotoContest.Models;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.ModelMappers
{
    public class ModelAutoMapper : Profile
    {
        public ModelAutoMapper()
        {
            CreateMap<UserDTO, UserViewModel>().ReverseMap();
            CreateMap<UserDTO, UserInputViewModel>().ReverseMap();
            CreateMap<ContestCategoryDTO, ContestCategoryViewModel>().ReverseMap();
            CreateMap<PhaseDTO, PhaseViewModel>().ReverseMap();
            CreateMap<ContestStateDTO, ContestStateViewModel>().ReverseMap();
            CreateMap<ContestDTO, ContestViewModel>().ReverseMap();
            //CreateMap<ContestInputDTO, ContestFrontViewModel>().ReverseMap();
            CreateMap<ContestDTO, ContestFrontViewModel>().ReverseMap();
            CreateMap<ContestDTO, ContestInputViewModel>().ReverseMap();
            CreateMap<ContestInputDTO, ContestCreateFrontViewModel>().ReverseMap();
            CreateMap<ContestOutputFrontDTO, ContestCreateFrontViewModel>().ReverseMap();
            CreateMap<ContestOutputFrontDTO, ContestInputDTO>().ReverseMap();
            CreateMap<PictureDTO, PictureViewModel>().ReverseMap();
            CreateMap<PictureDTO, PictureInputViewModel>().ReverseMap();
            CreateMap<PictureDTO, PictureFrontViewModel>().ReverseMap();
            CreateMap<PictureReviewDTO, PictureReviewViewModel>().ReverseMap();
            CreateMap<PictureReviewDTO, PictureReviewIFrontViewModel>().ReverseMap();
            CreateMap<UserViewDTO, UserViewModel>().ReverseMap();
            CreateMap<EnrollContestDTO, EnrollViewModel>().ReverseMap();

        }
    }
}