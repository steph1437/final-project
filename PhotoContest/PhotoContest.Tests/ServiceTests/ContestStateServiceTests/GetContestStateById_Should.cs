﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Data.Models;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.GetContestStateById_Should
{
    [TestClass]
    public class GetContestStateById_Should : BaseTest
    {

        [TestMethod]
        public async Task GetContestStateByIdShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(GetContestStateByIdShould));
            var contestStateId = 1;
            var contestState = new ContestState();

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
                contestState = await arrCtx.ContestStates
                    .FirstOrDefaultAsync(u => u.Id == contestStateId);
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {

                var sut = new ContestStateService(mapper, actCtx);
                var result = await sut.GetContestStateAsync(contestStateId);

                //Assert
                Assert.AreEqual(contestState.Name, result.Name);
                Assert.IsInstanceOfType(result, typeof(ContestStateDTO));
            }
        }

    }
}

