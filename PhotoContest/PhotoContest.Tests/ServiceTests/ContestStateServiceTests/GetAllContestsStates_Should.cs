﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Data.Models;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.GetAllContestsStates_Should
{
    [TestClass]
    public class GetAllContestsStates_Should : BaseTest
    {

        [TestMethod]
        public async Task GetAllContestsStatesShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(GetAllContestsStatesShould));
            var contestStates = new List<ContestState>();

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
                contestStates = await arrCtx.ContestStates
                    .Where(u => u.IsDeleted == false).ToListAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {

                var sut = new ContestStateService(mapper, actCtx);
                var result = await sut.GetAllContestStatesAsync();

                //Assert
                Assert.AreEqual(contestStates.Count, result.Count);
            }
        }

    }
}

