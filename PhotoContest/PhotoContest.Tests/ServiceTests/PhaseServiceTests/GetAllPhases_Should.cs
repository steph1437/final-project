﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Data.Models;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.GetAllPhases_Should
{
    [TestClass]
    public class GetAllPhases_Should : BaseTest
    {

        [TestMethod]
        public async Task GetAllPhasesShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(GetAllPhasesShould));
            var phases = new List<Phase>();

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
                phases = await arrCtx.Phases
                    .Where(u => u.IsDeleted == false).ToListAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {

                var sut = new PhaseService(mapper, actCtx);
                var result = await sut.GetAllContestPhasesAsync();

                //Assert
                Assert.AreEqual(phases.Count, result.Count);
            }
        }

    }
}

