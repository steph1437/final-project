﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Data.Models;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.GetPhaseByIdById_Should
{
    [TestClass]
    public class GetPhaseByIdById_Should : BaseTest
    {

        [TestMethod]
        public async Task GetPhaseByIdByIdShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(GetPhaseByIdByIdShould));
            var phaseId = 1;
            var phase = new Phase();

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
                phase = await arrCtx.Phases
                    .FirstOrDefaultAsync(u => u.Id == phaseId);
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {

                var sut = new PhaseService(mapper, actCtx);
                var result = await sut.GetContestPhaseAsync(phaseId);

                //Assert
                Assert.AreEqual(phase.Name, result.Name);
                Assert.IsInstanceOfType(result, typeof(PhaseDTO));
            }
        }

    }
}

