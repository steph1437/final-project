﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.DeletePicture_Should
{
    [TestClass]
    public class DeletePicture_Should : BaseTest
    {

        [TestMethod]
        public async Task DeletePictureShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(DeletePictureShould));
            var pictures = new List<Picture>();

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
                pictures = await arrCtx.Pictures
                    .Where(u => u.IsDeleted == false).ToListAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {
                var sut = new PictureService(mapper, actCtx, dateTimeProvider);
                sut.DeletePictureAsync(1);
                var result = await actCtx.Pictures
                    .Where(u => u.IsDeleted == false).ToListAsync();
                var secondResult = await actCtx.Pictures.FirstOrDefaultAsync(u => u.Id == 1);

                //Assert
                Assert.AreEqual(pictures.Count - 1, result.Count);
                Assert.AreEqual(secondResult.IsDeleted, true);
            }
        }

    }
}

