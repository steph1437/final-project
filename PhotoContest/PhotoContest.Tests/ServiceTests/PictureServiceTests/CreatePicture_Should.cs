﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.CreatePicture_Should
{
    [TestClass]
    public class CreatePicture_Should : BaseTest
    {

        [TestMethod]
        public async Task CreatePictureShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(CreatePictureShould));
            var contestDTO = new ContestDTO { Id = 2123 };
            var picture = new PictureDTO
            {
                Name = "Stefan",
                Url = "Zhuntovski",
                Contest = contestDTO
            };

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {
                var sut = new PictureService(mapper, actCtx, dateTimeProvider);
                var result = await sut.CreatePictureAsync(picture);
                await actCtx.SaveChangesAsync();
                var tester = actCtx.Pictures.LastOrDefault().Name;
                //Assert
                Assert.AreEqual(tester, "Stefan"); ;
            }
        }

    }
}

