﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.GetAllPictures_Should
{
    [TestClass]
    public class GetAllPictures_Should : BaseTest
    {

        [TestMethod]
        public async Task GetAllPicturesShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(GetAllPicturesShould));
            var pictures = new List<Picture>();

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
                pictures = await arrCtx.Pictures
                    .Where(u => u.IsDeleted == false).ToListAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {

                var sut = new PictureService(mapper, actCtx, dateTimeProvider);
                var result = await sut.GetAllPicturesAsync();

                //Assert
                Assert.AreEqual(pictures.Count, result.Count);
                Assert.AreEqual(result[0].Name, "Fashion Photo");
            }
        }

    }
}

