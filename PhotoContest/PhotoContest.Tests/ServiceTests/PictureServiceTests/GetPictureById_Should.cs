﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.GetPictureById_Should
{
    [TestClass]
    public class GetPictureById_Should : BaseTest
    {

        [TestMethod]
        public async Task ReturnCorrectPictureWithSpecificId()
        {
            //Arrange
            var options = Util.GetOptions(nameof(ReturnCorrectPictureWithSpecificId));
            var pictureId = 1;
            var picture = new Picture();

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
                picture = await arrCtx.Pictures
                    .FirstOrDefaultAsync(u => u.Id == pictureId);
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {

                var sut = new PictureService(mapper, actCtx, dateTimeProvider);
                var result = await sut.GetPictureAsync(pictureId);

                //Assert
                Assert.AreEqual(picture.Name, result.Name);
                Assert.IsInstanceOfType(result, typeof(PictureDTO));
            }
        }

    }
}

