﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.CreateContestCategory_Should
{
    [TestClass]
    public class CreateContestCategory_Should : BaseTest
    {

        [TestMethod]
        public async Task CreateContestCategoryShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(CreateContestCategoryShould));

            var user = new ContestCategoryDTO
            {
                Name = "Stefan"
            };

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {
                var sut = new ContestCategoryService(mapper, actCtx, dateTimeProvider);
                var result = await sut.CreateContestCategoryAsync(user);
                await actCtx.SaveChangesAsync();
                var tester = actCtx.ContestCategories.LastOrDefault().Name;
                //Assert
                Assert.AreEqual(tester, "Stefan"); ;
            }
        }
    }
}