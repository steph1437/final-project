﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.GetUserById_Should
{
    [TestClass]
    public class UpdateContestCategory_Should : BaseTest
    {

        [TestMethod]
        public async Task UpdateContestCategoryShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(UpdateContestCategoryShould));

            var user = new ContestCategoryDTO
            {
                Id = 1,
                Name = "fantasy"
            };

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {
                var sut = new ContestCategoryService(mapper, actCtx, dateTimeProvider);
                await sut.UpdateContestCategoryAsync(user);
                await actCtx.SaveChangesAsync();
                var tester = actCtx.ContestCategories.FirstOrDefault(u => u.Id == 1);
                //Assert
                Assert.AreEqual(tester.Name, "fantasy"); ;
            }
        }
    }
}