﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.GetContestCategoryById_Should
{
    [TestClass]
    public class GetContestCategoryById_Should : BaseTest
    {

        [TestMethod]
        public async Task GetContestCategoryByIdShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(GetContestCategoryByIdShould));
            var contestCategoryId = 1;
            var contestCategory = new ContestCategory();

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
                contestCategory = await arrCtx.ContestCategories
                    .FirstOrDefaultAsync(u => u.Id == contestCategoryId);
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {

                var sut = new ContestCategoryService(mapper, actCtx, dateTimeProvider);
                var result = await sut.GetContestCategoryAsync(contestCategoryId);

                //Assert
                Assert.AreEqual(contestCategory.Name, result.Name);
            }
        }

    }
}
