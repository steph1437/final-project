﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.DeleteContestCategory_Should
{
    [TestClass]
    public class DeleteContestCategory_Should : BaseTest
    {

        [TestMethod]
        public async Task DeleteContestCategoryShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(DeleteContestCategoryShould));
            var contestCategory = new List<ContestCategory>();

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
                contestCategory = await arrCtx.ContestCategories
                    .Where(u => u.IsDeleted == false).ToListAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {
                var sut = new ContestCategoryService(mapper, actCtx, dateTimeProvider);
                await sut.DeleteContestCategoryAsync(1);
                var result = await actCtx.ContestCategories
                    .Where(u => u.IsDeleted == false).ToListAsync();
                var secondResult = await actCtx.ContestCategories.FirstOrDefaultAsync(u => u.Id == 1);

                //Assert
                Assert.AreEqual(contestCategory.Count - 1, result.Count);
                Assert.AreEqual(secondResult.IsDeleted, true);
            }
        }

    }
}

