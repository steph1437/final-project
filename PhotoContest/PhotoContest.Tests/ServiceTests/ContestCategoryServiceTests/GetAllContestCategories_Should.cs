﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.GetAllContestCategories_Should
{
    [TestClass]
    public class GetAllContestCategories_Should : BaseTest
    {

        [TestMethod]
        public async Task GetAllContestCategoriesShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(GetAllContestCategoriesShould));
            var contestCategories = new List<ContestCategory>();

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
                contestCategories = await arrCtx.ContestCategories
                    .Where(u => u.IsDeleted == false).ToListAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {

                var sut = new ContestCategoryService(mapper, actCtx, dateTimeProvider);
                var result = await sut.GetAllContestCategoriesAsync();

                //Assert
                Assert.AreEqual(contestCategories.Count, result.Count);
            }
        }

    }
}
