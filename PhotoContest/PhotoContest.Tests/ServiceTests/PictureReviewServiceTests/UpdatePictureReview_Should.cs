﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.UpdatePictureReview_Should
{
    [TestClass]
    public class UpdatePictureReview_Should : BaseTest
    {

        [TestMethod]
        public async Task UpdatePictureReviewShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(UpdatePictureReviewShould));

            var PictureReview = new PictureReviewDTO
            {
                Id = 1,
                Comment = "haha"
            };

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {
                var sut = new ReviewService(mapper, actCtx, dateTimeProvider);
                await sut.UpdatePictureReviewAsync(PictureReview);
                await actCtx.SaveChangesAsync();
                var tester = actCtx.PictureReviews.FirstOrDefault(u => u.Id == 1);
                //Assert
                Assert.AreEqual(tester.Comment, "haha"); ;
            }
        }

       /* [TestMethod]
        public async Task UpdatePictureShouldThrowWhenContestDoesNotExists()
        {
            //Arrange
            var options = Util.GetOptions(nameof(UpdatePictureShouldThrowWhenContestDoesNotExists));

            var picture = new PictureDTO
            {
                Name = "Stefan",
                Url = "Zhuntovski",
                ContestId = 123
            };

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {
                var sut = new PictureService(mapper, actCtx, dateTimeProvider);

                //Assert
                await Assert.ThrowsExceptionAsync<ResourceNotExistException>(() => sut.UpdatePictureAsync(picture));
            }
        }*/
    }
}