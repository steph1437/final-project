﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.DeletePictureReview_Should
{
    [TestClass]
    public class DeletePictureReview_Should : BaseTest
    {

        [TestMethod]
        public async Task DeletePictureReviewShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(DeletePictureReviewShould));
            var pictureReviews = new List<PictureReview>();

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
                pictureReviews = await arrCtx.PictureReviews
                    .Where(u => u.IsDeleted == false).ToListAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {
                var sut = new ReviewService(mapper, actCtx, dateTimeProvider);
                await sut.DeletePictureReviewAsync(1);
                var result = await actCtx.PictureReviews
                    .Where(u => u.IsDeleted == false).ToListAsync();
                var secondResult = await actCtx.PictureReviews.FirstOrDefaultAsync(u => u.Id == 1);

                //Assert
                Assert.AreEqual(pictureReviews.Count - 1, result.Count);
                Assert.AreEqual(secondResult.IsDeleted, true);
            }
        }

    }
}

