﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.GetPictureReviewById_Should
{
    [TestClass]
    public class GetPictureReviewById_Should : BaseTest
    {

        [TestMethod]
        public async Task GetPictureReviewByIdShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(GetPictureReviewByIdShould));
            var pictureReviewId = 1;
            var pictureReview = new PictureReview();

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
                pictureReview = await arrCtx.PictureReviews
                    .FirstOrDefaultAsync(u => u.Id == pictureReviewId);
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {

                var sut = new ReviewService(mapper, actCtx, dateTimeProvider);
                var result = await sut.GetPictureReviewAsync(pictureReviewId);

                //Assert
                Assert.AreEqual(pictureReview.Score, result.Score);
                Assert.IsInstanceOfType(result, typeof(PictureReviewDTO));
            }
        }

    }
}

