﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.GetAllPictureReviews_Should
{
    [TestClass]
    public class GetAllPictureReviews_Should : BaseTest
    {

        [TestMethod]
        public async Task GetAllPictureReviewsShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(GetAllPictureReviewsShould));
            var pictureReviews = new List<PictureReview>();

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
                pictureReviews = await arrCtx.PictureReviews
                    .Where(u => u.IsDeleted == false).ToListAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {

                var sut = new ReviewService(mapper, actCtx, dateTimeProvider);
                var result = await sut.GetAllPictureReviewsAsync();

                //Assert
                Assert.AreEqual(pictureReviews.Count, result.Count);
                Assert.AreEqual(result[0].Score, 5.54);
            }
        }

    }
}

