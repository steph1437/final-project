﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.CreatePictureReview_Should
{
    [TestClass]
    public class CreatePictureReview_Should : BaseTest
    {

        [TestMethod]
        public async Task CreatePictureReviewShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(CreatePictureReviewShould));

            var PictureReview = new PictureReviewDTO
            {
                Comment = "haha",
                DoesntFitsCategoryCheckBox = false,
                JuryContestContestId = 1,
                PictureId = 1
            };

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {
                var sut = new ReviewService(mapper, actCtx, dateTimeProvider);
                await sut.CreatePictureReviewAsync(PictureReview);
                await actCtx.SaveChangesAsync();
                var tester = actCtx.PictureReviews.LastOrDefault();
                //Assert
                Assert.AreEqual(tester.Comment, "haha"); ;
            }
        }

    }
}

