﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.GetAllContests_Should
{
    [TestClass]
    public class GetAllContests_Should : BaseTest
    {

        [TestMethod]
        public async Task GetAllContestsShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(GetAllContestsShould));
            var contests = new List<Contest>();

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
                contests = await arrCtx.Contests
                    .Where(u => u.IsDeleted == false).ToListAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {

                var sut = new ContestService(mapper, actCtx, dateTimeProvider);
                var result = await sut.GetAllContestsAsync();

                //Assert
                Assert.AreEqual(contests.Count, result.Count);
            }
        }

    }
}
