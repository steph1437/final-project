﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.GetContestById_Should
{
    [TestClass]
    public class GetContestById_Should : BaseTest
    {

        [TestMethod]
        public async Task GetContestByIdShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(GetContestByIdShould));
            var contestId = 1;
            var contest = new Contest();

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
                contest = await arrCtx.Contests
                    .FirstOrDefaultAsync(u => u.Id == contestId);
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {

                var sut = new ContestService(mapper, actCtx, dateTimeProvider);
                var result = await sut.GetContestAsync(contestId);

                //Assert
                Assert.AreEqual(contest.Title, result.Title);
                Assert.IsInstanceOfType(result, typeof(ContestDTO));
            }
        }

    }
}

