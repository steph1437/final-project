﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.DeleteContest_Should
{
    [TestClass]
    public class DeleteContest_Should : BaseTest
    {

        [TestMethod]
        public async Task DeleteContestShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(DeleteContestShould));
            var contest = new List<Contest>();

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
                contest = await arrCtx.Contests
                    .Where(u => u.IsDeleted == false).ToListAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {
                var sut = new ContestService(mapper, actCtx, dateTimeProvider);
                await sut.DeleteContestAsync(1);
                var result = await actCtx.Contests
                    .Where(u => u.IsDeleted == false).ToListAsync();
                var secondResult = await actCtx.Contests.FirstOrDefaultAsync(u => u.Id == 1);

                //Assert
                Assert.AreEqual(contest.Count - 1, result.Count);
                Assert.AreEqual(secondResult.IsDeleted, true);
            }
        }

    }
}

