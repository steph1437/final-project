﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using PhotoContest.Services.Providers;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.CreateContestAsync_Should
{
    [TestClass]
    public class CreateContestAsync_Should : BaseTest
    {

        [TestMethod]
        public async Task CreateContestAsyncShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(CreateContestAsyncShould));
            var phaseProvider = new PhaseProvider
            {
                PhaseOne_StartDate = DateTime.Now,
                PhaseOne_EndDate = DateTime.Now.AddHours(24),
                PhaseTwo_StartDate = DateTime.Now.AddHours(24),
                PhaseTwo_Duration = 5
            };
            var contest = new ContestInputDTO
            {
                Id = 15,
                Title = "haha",
                Phases = phaseProvider,
                ContestStateId = 2,
                ContestCategoryPhaseDescription = "hehe",
                ContestCategoryId = 1
            };

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {
                var sut = new ContestService(mapper, actCtx, dateTimeProvider);
                var result = await sut.CreateAsync(contest);
                await actCtx.SaveChangesAsync();
                var tester = actCtx.Contests.LastOrDefault().Title;
                //Assert
                Assert.AreEqual(tester, "haha"); ;
            }
        }

    }
}

