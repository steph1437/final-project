﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.UpdateContest_Should
{
    [TestClass]
    public class UpdateContest_Should : BaseTest
    {

        [TestMethod]
        public async Task UpdateContestShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(UpdateContestShould));

            var contest = new ContestDTO
            {
                Id = 1,
                Title = "haha",
                ContestStateId = 2,
                ContestCategoryPhaseDescription = "hehe",
                ContestCategoryId = 1
            };

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {
                var sut = new ContestService(mapper, actCtx, dateTimeProvider);
                await sut.UpdateContestAsync(contest);
                await actCtx.SaveChangesAsync();
                var tester = actCtx.Contests.FirstOrDefault(u => u.Id == 1);
                //Assert
                Assert.AreEqual(tester.Title, "haha"); ;
            }
        }

        
        
    }
}