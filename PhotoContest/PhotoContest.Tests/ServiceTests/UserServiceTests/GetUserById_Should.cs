﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.GetUserById_Should
{
    [TestClass]
    public class GetUserById_Should : BaseTest
    {

        [TestMethod]
        public async Task ReturnCorrectUserWithSpecificId()
        {
            //Arrange
            var options = Util.GetOptions(nameof(ReturnCorrectUserWithSpecificId));
            var userId = 4;
            var user = new User();

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
                user = await arrCtx.Users
                    .FirstOrDefaultAsync(u => u.Id == userId);
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {

                var sut = new UserService(mapper, actCtx, dateTimeProvider);
                var result = await sut.GetUserAsync(userId);

                //Assert
                Assert.AreEqual(user.FirstName, result.FirstName);
                Assert.IsInstanceOfType(result, typeof(UserDTO));
            }
        }

    }
}

