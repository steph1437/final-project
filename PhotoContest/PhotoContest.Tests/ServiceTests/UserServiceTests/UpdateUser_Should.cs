﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.GetUserById_Should
{
    [TestClass]
    public class UpdateUser_Should : BaseTest
    {

        [TestMethod]
        public async Task UpdateUserShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(UpdateUserShould));

            var user = new UserDTO
            {
                Id = 1,
                FirstName = "Stefan",
                LastName = "Zhuntovski",
                UserName = "fantasy",
                Email = "st.zhuntovski@gmail.com"
            };

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {
                var sut = new UserService(mapper, actCtx, dateTimeProvider);
                sut.UpdateUserAsync(user);
                await actCtx.SaveChangesAsync();
                var tester = actCtx.Users.FirstOrDefault(u => u.Id == 1);
                //Assert
                Assert.AreEqual(tester.UserName, "fantasy"); ;
            }
        }

       /* [TestMethod]
        public async Task UpdateUserShouldThrowWhenUserDoesNotExist()
        {
            //Arrange
            var options = Util.GetOptions(nameof(UpdateUserShouldThrowWhenUserDoesNotExist));

            var user = new UserDTO
            {
                Id = 15,
                FirstName = "Stefan",
                LastName = "Zhuntovski",
                UserName = "fantasy",
                Email = "st.zhuntovski@gmail.com"
            };

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {
                var sut = new UserService(mapper, actCtx, dateTimeProvider);

                //Assert
                Assert.ThrowsExceptionAsync<ResourceNotExistException>(() => sut.UpdateUserAsync(user));
            }
        }*/
    }

}