﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using PhotoContest.Services.Helpers;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.GetUserById_Should
{
    [TestClass]
    public class CreateUser_Should : BaseTest
    {

        [TestMethod]
        public async Task CreateUserShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(CreateUserShould));
            
            var user = new UserDTO 
            {
                FirstName = "Stefan",
                LastName = "Zhuntovski",
                UserName = "fantasy",
                Email = "st.zhuntovski@gmail.com"
            };

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {
                

                var sut = new UserService(mapper, actCtx, dateTimeProvider);
                var result = await sut.CreateUserAsync(user);
                await actCtx.SaveChangesAsync();
                var tester = actCtx.Users.LastOrDefault().FirstName;
                //Assert
                Assert.AreEqual(tester, "Stefan"); ;
            }
        }

        [TestMethod]
        public async Task CreateUserShouldThrowWhenEmailExists()
        {
            //Arrange
            var options = Util.GetOptions(nameof(CreateUserShouldThrowWhenEmailExists));

            var user = new UserDTO
            {
                FirstName = "Stefan",
                LastName = "Zhuntovski",
                UserName = "fantasy",
                Email = "donkihot@abv.bg"
            };

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {
                var sut = new UserService(mapper, actCtx, dateTimeProvider);

                //Assert
                await Assert.ThrowsExceptionAsync<ResourseAlreadyExistException>(() => sut.CreateUserAsync(user));
            }
        }
        [TestMethod]
        public async Task CreateUserShouldThrowWhenInvalidEmail()
        {
            //Arrange
            var options = Util.GetOptions(nameof(CreateUserShouldThrowWhenInvalidEmail));

            var user = new UserDTO
            {
                FirstName = "Stefan",
                LastName = "Zhuntovski",
                UserName = "dkihot@abv.bg",
                Email = "donkihсt@abv.bg"
            };

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {
                var sut = new UserService(mapper, actCtx, dateTimeProvider);

                //Assert
                await Assert.ThrowsExceptionAsync<InvalidParameterException>(() => sut.CreateUserAsync(user));
            }
        }

        [TestMethod]
        public async Task CreateUserShouldThrowWhenUsernameTaken()
        {
            //Arrange
            var options = Util.GetOptions(nameof(CreateUserShouldThrowWhenUsernameTaken));

            var user = new UserDTO
            {
                FirstName = "Stefan",
                LastName = "Zhuntovski",
                UserName = "donkihot@abv.bg",
                Email = "don@abv.bg"
            };

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {
                var sut = new UserService(mapper, actCtx, dateTimeProvider);

                //Assert
                await Assert.ThrowsExceptionAsync<ResourseAlreadyExistException>(() => sut.CreateUserAsync(user));
            }
        }
    }
}

