﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.Helpers;
using PhotoContest.Services.Services;
using PhotoContest.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Tests.ServiceTests.DeleteUser_Should
{
    [TestClass]
    public class DeleteUser_Should : BaseTest
    {

        [TestMethod]
        public async Task DeleteUserShould()
        {
            //Arrange
            var options = Util.GetOptions(nameof(DeleteUserShould));
            var users = new List<User>();

            using (var arrCtx = new PhotoContestContext(options))
            {
                arrCtx.SeedData();
                await arrCtx.SaveChangesAsync();
                users = await arrCtx.Users
                    .Where(u => u.IsDeleted == false).ToListAsync();
            }

            //Act
            using (var actCtx = new PhotoContestContext(options))
            {
                    
                var sut = new UserService(mapper, actCtx, dateTimeProvider);
                await sut.DeleteUserAsync(1);
                var result = await actCtx.Users
                    .Where(u => u.IsDeleted == false).ToListAsync();
                var secondResult = await actCtx.Users.FirstOrDefaultAsync(u => u.Id == 1);

                //Assert
                Assert.AreEqual(users.Count-1, result.Count);
                Assert.AreEqual(secondResult.IsDeleted, true);
            }
        }

    }
}

