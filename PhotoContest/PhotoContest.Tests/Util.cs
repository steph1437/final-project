using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Data.Models;
using System;
using System.Collections.Generic;

namespace PhotoContest.Test
{
    public static class Util
    {
        public static DbContextOptions<PhotoContestContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<PhotoContestContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }

        public static void SeedData(this PhotoContestContext context)
        {
            var roles = new List<Role>
            {
                new Role { Id = 1, Name = "Admin", NormalizedName = "ADMIN" },
                new Role { Id = 2, Name = "Master", NormalizedName = "MASTER" },
                new Role { Id = 3, Name = "Jury", NormalizedName = "JURY" },
                new Role { Id = 4, Name = "Participant", NormalizedName = "PARTICIPANT" },
                new Role { Id = 5, Name = "Organizer", NormalizedName = "ORGANIZER" }
            };
            context.AddRange(roles);

            var identityUserRoles = new List<IdentityUserRole<int>>
            {
                 new IdentityUserRole<int>
                 {
                     RoleId = 1,
                     UserId = 1
                 },
                 new IdentityUserRole<int>
                 {
                     RoleId = 3,
                     UserId = 2,
                 },
                 new IdentityUserRole<int>
                 {
                     RoleId = 3,
                     UserId = 3
                 }
            };
            context.AddRange(identityUserRoles);

            var hasher = new PasswordHasher<User>();
            User managerUserr = new User
            {
                Id = 1,
                FirstName = "Admin",
                LastName = "Adminov",
                UserName = "fredSpred@mail.bg",
                NormalizedUserName = "FREDSPRED@MAIL.BG",
                Email = "fredSpred@mail.bg",
                NormalizedEmail = "fredSpred@mail.bg",
                LockoutEnabled = true,
                SecurityStamp = Guid.NewGuid().ToString()
            };
            managerUserr.PasswordHash = hasher.HashPassword(managerUserr, "Parola");

            var users = new List<User>
            {
                 new User
                 {
                     Id = 2,
                     FirstName = "Stivan",
                     LastName = "Ivanov",
                     UserName = "donkihot@abv.bg",
                     Email = "donkihot@abv.bg",
                 },
                 new User
                 {
                     Id = 3,
                     FirstName = "Georgi",
                     LastName = "Penev",
                     UserName = "supermario@abv.bg",
                     Email = "supermario@abv.bg",
                 },
                 new User
                 {
                     Id = 4,
                     FirstName = "Ivan",
                     LastName = "Ivanov",
                     UserName = "ivanivanov@abv.bg",
                     Email = "ivanivanov@abv.bg"
                 },
                 new User
                 {
                     Id = 5,
                     FirstName = "Georgi",
                     LastName = "Ivanov",
                     UserName = "georgiivanov@abv.bg",
                     Email = "georgiivanov@abv.bg"
                 }
            };
            users.Add(managerUserr);
            context.AddRange(users);

            var contestCategories = new List<ContestCategory>
            {
                  new ContestCategory(){Id = 1,Name = "Portrait Photography"},
                  new ContestCategory(){Id = 2,Name = "Fashion Photography"},
                  new ContestCategory(){Id = 3,Name = "Sports Photography"},
                  new ContestCategory(){Id = 4,Name = "Photojournalism"},
                  new ContestCategory(){Id = 5,Name = "Editorial Photography"},
                  new ContestCategory(){Id = 6,Name = "Architectural Photography"},
                  new ContestCategory(){Id = 7,Name = "Food Photography"},
                  new ContestCategory(){Id = 8,Name = "Adventure Photography"}
            };
            context.AddRange(contestCategories);

            /*var contestPhases = new List<ContestPhase>
            {
                  new ContestPhase(){Id = 1,Name="PhaseI"},
                  new ContestPhase(){Id = 2,Name="PhaseII"},
                  new ContestPhase(){Id = 3,Name="Finished"},
            };
            context.AddRange(contestPhases);*/

            var contestStates = new List<ContestState>
            {
                  new ContestState(){Id = 1,Name="Open"},
                  new ContestState(){Id = 2,Name="Invitational"}
             };
            context.AddRange(contestStates);

            var contests = new List<Contest>
            {
                  new Contest(){Id = 1, ContestCategoryId=2, ContestStateId=1, Title = "Fashion Faces"},
                  new Contest(){Id = 2, ContestCategoryId=4, ContestStateId=2, Title = "Photo Journalism "},
                  new Contest(){Id = 3, ContestCategoryId=7, ContestStateId=1, Title = "Foods "}
            };
            context.AddRange(contests);

            var pictures = new List<Picture>
            {
                  new Picture(){Id = 1, Url="https://expertphotography.com/wp-content/uploads/2019/04/fashion-photography-inspiration-6.jpg",ContestId=1,Name="Fashion Photo"},
                  new Picture(){Id = 2, Url="https://www.jdinstitute.edu.in/media/2019/03/Fashion-Photography-1.jpg",ContestId=1,Name="Fashion Photo"},
            };
            context.AddRange(pictures);

            var pictureReviews = new List<PictureReview>
            {
                  new PictureReview(){Id = 1, Comment="",PictureId=1,Score=5.54},
                  new PictureReview(){Id = 2, Comment="",PictureId=1,Score=3.46},
                  new PictureReview(){Id = 3, Comment="",PictureId=2,Score=7.46},
                  new PictureReview(){Id = 4, Comment="",PictureId=2,Score=5.26},
            };
            context.AddRange(pictureReviews);

            var Juries = new List<JuryContest>
            {
                  new JuryContest(){Id=1,JuryContestContestId=1,JuryContestUserId=1},
                  new JuryContest(){Id=2,JuryContestContestId=2,JuryContestUserId=1},
                  new JuryContest(){Id=3,JuryContestContestId=3,JuryContestUserId=1},
            };
            context.AddRange(Juries);

            var contestPhases = new List<ContestPhase>()
              {
                   // ONE
                        new ContestPhase()
                        {   Id = 1,
                            ContestId = 1,
                            PhaseId=1,
                            CreatedOn=DateTime.UtcNow,
                            EndDate=DateTime.UtcNow.AddDays(30)

                        },
                        new ContestPhase()
                        {
                            Id = 2,
                            ContestId = 1,
                            PhaseId=2,
                            CreatedOn=DateTime.UtcNow,
                            EndDate=DateTime.UtcNow.AddDays(30)

                        },
                       new ContestPhase()
                       {
                            Id = 3,
                            ContestId = 1,
                            PhaseId=2,
                            CreatedOn=DateTime.UtcNow,
                            EndDate=DateTime.UtcNow.AddDays(30)

                       },
                       //TWO
                        new ContestPhase()
                        {
                            Id = 4,
                            ContestId = 2,
                            PhaseId=1,
                            CreatedOn=DateTime.UtcNow.AddDays(-30),
                            EndDate=DateTime.UtcNow

                        },
                        new ContestPhase()
                        {
                            Id = 5,
                            ContestId = 2,
                            PhaseId=2,
                            CreatedOn=DateTime.UtcNow,
                            EndDate=DateTime.UtcNow.AddDays(30)

                        },
                       new ContestPhase()
                       {
                           Id = 6,
                            ContestId = 2,
                            PhaseId=3,
                            CreatedOn=DateTime.UtcNow.AddDays(30),
                            EndDate=DateTime.MaxValue

                       },
                       // FINISHED

                        new ContestPhase()
                        {
                            Id = 7,
                            ContestId = 3,
                            PhaseId=1,
                            CreatedOn=DateTime.UtcNow.AddDays(-60),
                            EndDate=DateTime.UtcNow.AddDays(-30)

                        },
                        new ContestPhase()
                        {
                            Id = 8,
                            ContestId = 3,
                            PhaseId=2,
                            CreatedOn=DateTime.UtcNow.AddDays(-30),
                            EndDate=DateTime.UtcNow

                        },
                       new ContestPhase()
                       {
                            Id=9,
                            ContestId = 3,
                            PhaseId=3,
                            CreatedOn=DateTime.UtcNow,
                            EndDate=DateTime.MaxValue

                       },

              };
            context.AddRange(contestPhases);

            var phases = new List<Phase>()
            {
                        new Phase(){Id = 1,Name="PhaseI"},
                        new Phase(){Id = 2,Name="PhaseII"},
                        new Phase(){Id = 3,Name="Finished"},
            };
            context.AddRange(phases);
        }
    }
}

