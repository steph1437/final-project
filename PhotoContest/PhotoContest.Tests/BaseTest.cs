﻿using AutoMapper;
using Moq;
using PhotoContest.Services.DTOMappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PhotoContest.Services.Providers.Contracts;
using PhotoContest.Data;
using Microsoft.AspNetCore.Identity;
using PhotoContest.Services.Helpers;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

public abstract class BaseTest
{
    protected IMapper mapper;
    protected IDatеTimeProvider dateTimeProvider;

    public BaseTest()
    {
      

        var config = new MapperConfiguration(opts => opts.AddProfile(new DTOAutoMapper()));
        mapper = config.CreateMapper();

        var mock = new Mock<IDatеTimeProvider>();
        mock.Setup(x => x.GetDateTime());
        dateTimeProvider = mock.Object;


    }
}
