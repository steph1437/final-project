﻿using PhotoContest.Data.Entities.Abstracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Entities
{
    public class Entity : IEntity
    {
        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }

    }
}
