﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Entities.Abstracts
{
    public interface IEntity
    {
        public DateTime CreatedOn { get; set; } 
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}
