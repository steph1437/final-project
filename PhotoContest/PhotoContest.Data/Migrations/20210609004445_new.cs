﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PhotoContest.Data.Migrations
{
    public partial class @new : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "8f9bab59-7e77-4436-b98f-e08d73a6437d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "165077c3-fecc-42fc-8d67-ddd8ecb06407");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "05916533-d3fd-4129-9030-0fb5d7b77a56");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "49109d4b-5221-4860-addb-b2caf63c0d58", "AQAAAAEAACcQAAAAEK1+nqqkQ5kd1SP9Bb7qjQBrbMCzulcvPUt5ZmKEu67neg0Gc7VLl00844C7iELgBQ==", "fbe932c1-c49f-4d82-80d4-b27b37179bb2" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "afc63ef0-fa0a-4307-b177-e54b8e1be31f", "10e14111-7771-4742-981b-8bc587663da7" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "a2c080f6-f2ec-425a-947c-20a29f71ac6c", "31bfdb4d-cc87-4312-b2f3-ef22b651d186" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "f8df4217-d9ba-4ed1-876e-adbd2492ddf5", "1e75f015-3770-468d-b500-e2e756150584" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "9b8f4e2f-14b2-4079-a8f5-5a20700ca5c9", "12c28165-8a9f-4907-84c2-6d473db672a2" });

            migrationBuilder.UpdateData(
                table: "ContenderContests",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 999, DateTimeKind.Utc).AddTicks(836));

            migrationBuilder.InsertData(
                table: "ContenderContests",
                columns: new[] { "Id", "ContestId", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "PictureId", "UserId" },
                values: new object[] { 2, 1, new DateTime(2021, 6, 9, 0, 44, 44, 999, DateTimeKind.Utc).AddTicks(3152), null, false, null, 2, 3 });

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 996, DateTimeKind.Utc).AddTicks(9751));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(951));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(977));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(977));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(977));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(977));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 7,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(977));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 8,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(977));

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(5706), new DateTime(2021, 5, 20, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6239), new DateTime(2021, 5, 10, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(5039) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6808), new DateTime(2021, 5, 30, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6826), new DateTime(2021, 5, 20, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6801) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedOn", "StartDate" },
                values: new object[] { new DateTime(2021, 7, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6833), new DateTime(2021, 5, 30, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6833) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6837), new DateTime(2021, 5, 20, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6837), new DateTime(2021, 5, 10, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6837) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6841), new DateTime(2021, 5, 30, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6841), new DateTime(2021, 5, 20, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6841) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedOn", "StartDate" },
                values: new object[] { new DateTime(2021, 7, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6841), new DateTime(2021, 5, 30, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6841) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6844), new DateTime(2021, 5, 20, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6844), new DateTime(2021, 5, 10, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6844) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6844), new DateTime(2021, 5, 30, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6844), new DateTime(2021, 5, 20, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6844) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedOn", "StartDate" },
                values: new object[] { new DateTime(2021, 7, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6848), new DateTime(2021, 5, 30, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6848) });

            migrationBuilder.UpdateData(
                table: "ContestStates",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(7803));

            migrationBuilder.UpdateData(
                table: "ContestStates",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(9163));

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 998, DateTimeKind.Utc).AddTicks(418));

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 4, 30, 0, 44, 44, 998, DateTimeKind.Utc).AddTicks(2839));

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 5, 30, 0, 44, 44, 998, DateTimeKind.Utc).AddTicks(2872));

            migrationBuilder.UpdateData(
                table: "JuryContests",
                keyColumns: new[] { "JuryContestUserId", "JuryContestContestId" },
                keyValues: new object[] { 1, 1 },
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 998, DateTimeKind.Utc).AddTicks(6708));

            migrationBuilder.UpdateData(
                table: "JuryContests",
                keyColumns: new[] { "JuryContestUserId", "JuryContestContestId" },
                keyValues: new object[] { 1, 2 },
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 998, DateTimeKind.Utc).AddTicks(9873));

            migrationBuilder.UpdateData(
                table: "JuryContests",
                keyColumns: new[] { "JuryContestUserId", "JuryContestContestId" },
                keyValues: new object[] { 1, 3 },
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 998, DateTimeKind.Utc).AddTicks(9895));

            migrationBuilder.UpdateData(
                table: "Phases",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(1856));

            migrationBuilder.UpdateData(
                table: "Phases",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(2855));

            migrationBuilder.UpdateData(
                table: "Phases",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(2873));

            migrationBuilder.UpdateData(
                table: "PictureReviews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 999, DateTimeKind.Utc).AddTicks(3921));

            migrationBuilder.UpdateData(
                table: "PictureReviews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 999, DateTimeKind.Utc).AddTicks(7068));

            migrationBuilder.UpdateData(
                table: "Pictures",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "Name", "Story" },
                values: new object[] { new DateTime(2021, 6, 9, 0, 44, 44, 998, DateTimeKind.Utc).AddTicks(3565), "Beautiful woman", "Extravagantly dressed woman." });

            migrationBuilder.UpdateData(
                table: "Pictures",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "Story" },
                values: new object[] { new DateTime(2021, 6, 9, 0, 44, 44, 998, DateTimeKind.Utc).AddTicks(5953), "A womman covering her eyes." });

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 996, DateTimeKind.Utc).AddTicks(7578));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 996, DateTimeKind.Utc).AddTicks(8851));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 996, DateTimeKind.Utc).AddTicks(8876));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 996, DateTimeKind.Utc).AddTicks(8876));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ContenderContests",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "aabe1034-ab4a-40ae-b2b2-dd0da95db30b");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "c73b4446-5a3c-46c0-b3b3-ec61cd30addb");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "8490f96d-c83c-4410-8211-548645869fd0");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "73efedb1-53d8-47ee-af1d-68ae5d8d804b", "AQAAAAEAACcQAAAAEKv2kxoJPK/0989TqjhJGd3ykdv8VmzApIuj4sPQcm8IH36lpkXj+gdt4x8xJz0Mbw==", "f5da46ec-1573-4131-8fd9-7533a09b6f6d" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "9f3e05c8-1a36-40ef-b582-f5f7f1eefa09", "6553ce53-d975-45a4-9e78-a912f5e683d9" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "be6402d2-3640-4b02-9919-eb7611d6e66c", "511edddf-bcac-40ab-85d4-398a95ccf31d" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "adacc750-157e-4c0d-a80b-47dd7f663657", "794659fd-65a1-490e-84f1-40aff12f8122" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "aa8e24b4-f30f-4e13-9287-86a152903551", "60013566-d945-4893-8158-a1ae3e9c4bf4" });

            migrationBuilder.UpdateData(
                table: "ContenderContests",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 40, DateTimeKind.Utc).AddTicks(7452));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(1130));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(2645));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(2724));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(2729));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(2731));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(2733));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 7,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(2735));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 8,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(2737));

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 9, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(9787), new DateTime(2021, 5, 19, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(526), new DateTime(2021, 5, 9, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(8645) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 9, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1324), new DateTime(2021, 5, 29, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1386), new DateTime(2021, 5, 19, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1297) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedOn", "StartDate" },
                values: new object[] { new DateTime(2021, 7, 8, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1405), new DateTime(2021, 5, 29, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1403) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 9, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1473), new DateTime(2021, 5, 19, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1476), new DateTime(2021, 5, 9, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1409) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 9, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1482), new DateTime(2021, 5, 29, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1484), new DateTime(2021, 5, 19, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1481) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedOn", "StartDate" },
                values: new object[] { new DateTime(2021, 7, 8, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1489), new DateTime(2021, 5, 29, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1488) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 9, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1501), new DateTime(2021, 5, 19, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1503), new DateTime(2021, 5, 9, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1499) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 9, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1508), new DateTime(2021, 5, 29, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1509), new DateTime(2021, 5, 19, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1506) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedOn", "StartDate" },
                values: new object[] { new DateTime(2021, 7, 8, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1514), new DateTime(2021, 5, 29, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1512) });

            migrationBuilder.UpdateData(
                table: "ContestStates",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(2950));

            migrationBuilder.UpdateData(
                table: "ContestStates",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(4602));

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(6747));

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 4, 29, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(9851));

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 5, 29, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(9914));

            migrationBuilder.UpdateData(
                table: "JuryContests",
                keyColumns: new[] { "JuryContestUserId", "JuryContestContestId" },
                keyValues: new object[] { 1, 1 },
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 40, DateTimeKind.Utc).AddTicks(4824));

            migrationBuilder.UpdateData(
                table: "JuryContests",
                keyColumns: new[] { "JuryContestUserId", "JuryContestContestId" },
                keyValues: new object[] { 1, 2 },
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 40, DateTimeKind.Utc).AddTicks(6366));

            migrationBuilder.UpdateData(
                table: "JuryContests",
                keyColumns: new[] { "JuryContestUserId", "JuryContestContestId" },
                keyValues: new object[] { 1, 3 },
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 40, DateTimeKind.Utc).AddTicks(6414));

            migrationBuilder.UpdateData(
                table: "Phases",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(4086));

            migrationBuilder.UpdateData(
                table: "Phases",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(5444));

            migrationBuilder.UpdateData(
                table: "Phases",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(5511));

            migrationBuilder.UpdateData(
                table: "PictureReviews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 41, DateTimeKind.Utc).AddTicks(760));

            migrationBuilder.UpdateData(
                table: "PictureReviews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 41, DateTimeKind.Utc).AddTicks(6673));

            migrationBuilder.UpdateData(
                table: "Pictures",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "Name", "Story" },
                values: new object[] { new DateTime(2021, 6, 8, 18, 52, 58, 40, DateTimeKind.Utc).AddTicks(991), "Beutifull women", "Extravagant dressed women" });

            migrationBuilder.UpdateData(
                table: "Pictures",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "Story" },
                values: new object[] { new DateTime(2021, 6, 8, 18, 52, 58, 40, DateTimeKind.Utc).AddTicks(3763), "wommed covering her eyse " });

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 37, DateTimeKind.Utc).AddTicks(8675));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 37, DateTimeKind.Utc).AddTicks(9916));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 37, DateTimeKind.Utc).AddTicks(9978));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 8, 18, 52, 58, 37, DateTimeKind.Utc).AddTicks(9980));
        }
    }
}
