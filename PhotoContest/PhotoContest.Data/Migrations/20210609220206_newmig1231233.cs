﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PhotoContest.Data.Migrations
{
    public partial class newmig1231233 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "c22d1492-8a31-4fd2-b1eb-8c3677ce1497");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "1df49c0e-b223-46f9-9ec2-df7bcafd1930");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "21dc5d08-9e32-43b5-bbae-aa4674e6959e");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "812bf761-48b1-433c-9e60-836dc125fcb0", "AQAAAAEAACcQAAAAEIsmPCZTsDwNJEqrMaJ86QDnvZhzYlvClxBxgVs7ycOBrWBc96cUgHre1wsG3UzsfA==", "e15d031f-9113-42a2-be86-922a1e239b46" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "28784c24-074b-43c6-8b7d-c3e0315f1b4a", "95ac492f-2862-4fe6-94fd-3ef90787a709" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "4a48079e-046c-4e03-8eb9-587b2546e6dc", "51dfeb10-583b-47b6-acec-32c2567dec3f" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "d26ce272-a2c1-4bec-9248-69cfaa412a73", "64a68f9c-9c14-4a1a-a40a-506503f7914b" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "a7bfd175-cf33-4fcc-974f-e520b50fadca", "254b78bd-5345-47be-9251-2a7464dda6d1" });

            migrationBuilder.UpdateData(
                table: "ContenderContests",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 798, DateTimeKind.Utc).AddTicks(1564));

            migrationBuilder.UpdateData(
                table: "ContenderContests",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 798, DateTimeKind.Utc).AddTicks(3979));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(1256));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(2550));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(2576));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(2580));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(2580));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(2580));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 7,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(2580));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 8,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(2580));

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 6, 8, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(7660), new DateTime(2021, 6, 9, 21, 2, 4, 796, DateTimeKind.Utc).AddTicks(8218), new DateTime(2021, 6, 8, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(6916) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(8834), new DateTime(2021, 6, 10, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(8860), new DateTime(2021, 6, 9, 21, 2, 4, 796, DateTimeKind.Utc).AddTicks(8827) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedOn", "StartDate" },
                values: new object[] { new DateTime(2021, 7, 9, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(8871), new DateTime(2021, 6, 10, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(8871) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(8875), new DateTime(2021, 5, 20, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(8875), new DateTime(2021, 5, 10, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(8875) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(8878), new DateTime(2021, 5, 30, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(8878), new DateTime(2021, 5, 20, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(8875) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedOn", "StartDate" },
                values: new object[] { new DateTime(2021, 7, 9, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(8878), new DateTime(2021, 5, 30, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(8878) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(8882), new DateTime(2021, 5, 20, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(8882), new DateTime(2021, 5, 10, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(8878) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(8882), new DateTime(2021, 5, 30, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(8882), new DateTime(2021, 5, 20, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(8882) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedOn", "StartDate" },
                values: new object[] { new DateTime(2021, 7, 9, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(8886), new DateTime(2021, 5, 30, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(8882) });

            migrationBuilder.UpdateData(
                table: "ContestStates",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(9735));

            migrationBuilder.UpdateData(
                table: "ContestStates",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 797, DateTimeKind.Utc).AddTicks(800));

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 797, DateTimeKind.Utc).AddTicks(2259));

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ContestCategoryPhaseDescription", "CreatedOn", "Foreground_Url", "Title" },
                values: new object[] { "Finished", new DateTime(2021, 4, 30, 22, 2, 4, 797, DateTimeKind.Utc).AddTicks(4873), "https://www.ecmwf.int/sites/default/files/styles/news_item_main_image/public/ThinkstockPhotos-Lightning-146914924-690px.jpg?itok=BClSyctU", "Lightning Contest" });

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedOn", "Foreground_Url" },
                values: new object[] { new DateTime(2021, 5, 30, 22, 2, 4, 797, DateTimeKind.Utc).AddTicks(4914), "http://www.natureimagesawards.com/wp-content/uploads/2019/05/humming-bird.jpg" });

            migrationBuilder.UpdateData(
                table: "JuryContests",
                keyColumns: new[] { "JuryContestUserId", "JuryContestContestId" },
                keyValues: new object[] { 1, 1 },
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 797, DateTimeKind.Utc).AddTicks(9063));

            migrationBuilder.UpdateData(
                table: "JuryContests",
                keyColumns: new[] { "JuryContestUserId", "JuryContestContestId" },
                keyValues: new object[] { 1, 2 },
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 798, DateTimeKind.Utc).AddTicks(573));

            migrationBuilder.UpdateData(
                table: "JuryContests",
                keyColumns: new[] { "JuryContestUserId", "JuryContestContestId" },
                keyValues: new object[] { 1, 3 },
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 798, DateTimeKind.Utc).AddTicks(598));

            migrationBuilder.UpdateData(
                table: "Phases",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(3480));

            migrationBuilder.UpdateData(
                table: "Phases",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(4560));

            migrationBuilder.UpdateData(
                table: "Phases",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(4574));

            migrationBuilder.UpdateData(
                table: "PictureReviews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 798, DateTimeKind.Utc).AddTicks(4865));

            migrationBuilder.UpdateData(
                table: "PictureReviews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 798, DateTimeKind.Utc).AddTicks(8114));

            migrationBuilder.UpdateData(
                table: "Pictures",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 797, DateTimeKind.Utc).AddTicks(5694));

            migrationBuilder.UpdateData(
                table: "Pictures",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 797, DateTimeKind.Utc).AddTicks(8231));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 795, DateTimeKind.Utc).AddTicks(9017));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(311));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(337));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 22, 2, 4, 796, DateTimeKind.Utc).AddTicks(337));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "fe4f92de-8d11-49b3-a195-1a2f8b38d4af");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "6ce7e66f-0169-421c-a2e2-dcf85865b07d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "5be08704-30ed-4529-9cff-8f762f703512");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "cfd9cd30-21a3-4cae-ae8b-2b92a4e83c8c", "AQAAAAEAACcQAAAAEPBPxRtIU0qVaWfqstHoaCY4AzkXvJaS97AzhWJ3keDIqJoU28+vCjJVI09FfvMthQ==", "979ac41e-9a03-4f3d-8a65-cbd38ef3f0f3" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "9623d809-8971-4c5b-93ee-c5482b19dd78", "dd22e74e-b78c-4772-95bd-8a4ef48ee147" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "25f0261c-91b4-4d5d-a0a2-907349d85e06", "7f973c18-6b39-44a3-8bd6-0db35d04b3f6" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "ed7745f5-cf93-4a48-ae2a-2b3f9a21eeaa", "18801aae-f280-4a4a-a7d8-ea9d10ea7901" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "1a8b52d5-69fb-4090-90d5-1069b5748eb1", "8108e589-023a-41d8-b42e-88c02e07f038" });

            migrationBuilder.UpdateData(
                table: "ContenderContests",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 317, DateTimeKind.Utc).AddTicks(5912));

            migrationBuilder.UpdateData(
                table: "ContenderContests",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 317, DateTimeKind.Utc).AddTicks(8407));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(6596));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(7879));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(7905));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(7905));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(7905));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(7909));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 7,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(7909));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 8,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(7909));

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(2781), new DateTime(2021, 5, 20, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3331), new DateTime(2021, 5, 10, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(2091) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3911), new DateTime(2021, 5, 30, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3933), new DateTime(2021, 5, 20, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3904) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedOn", "StartDate" },
                values: new object[] { new DateTime(2021, 7, 9, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3940), new DateTime(2021, 5, 30, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3940) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3944), new DateTime(2021, 5, 20, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3944), new DateTime(2021, 5, 10, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3944) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3948), new DateTime(2021, 5, 30, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3948), new DateTime(2021, 5, 20, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3944) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedOn", "StartDate" },
                values: new object[] { new DateTime(2021, 7, 9, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3948), new DateTime(2021, 5, 30, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3948) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3951), new DateTime(2021, 5, 20, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3951), new DateTime(2021, 5, 10, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3948) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3951), new DateTime(2021, 5, 30, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3951), new DateTime(2021, 5, 20, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3951) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedOn", "StartDate" },
                values: new object[] { new DateTime(2021, 7, 9, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3951), new DateTime(2021, 5, 30, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3951) });

            migrationBuilder.UpdateData(
                table: "ContestStates",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(4739));

            migrationBuilder.UpdateData(
                table: "ContestStates",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(5792));

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(7149));

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ContestCategoryPhaseDescription", "CreatedOn", "Foreground_Url", "Title" },
                values: new object[] { "Phase Two", new DateTime(2021, 4, 30, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(9607), "https://images.unsplash.com/photo-1522335789203-aabd1fc54bc9?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1065&q=80", "Fashion Faces" });

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedOn", "Foreground_Url" },
                values: new object[] { new DateTime(2021, 5, 30, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(9647), "https://images.unsplash.com/photo-1522335789203-aabd1fc54bc9?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1065&q=80" });

            migrationBuilder.UpdateData(
                table: "JuryContests",
                keyColumns: new[] { "JuryContestUserId", "JuryContestContestId" },
                keyValues: new object[] { 1, 1 },
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 317, DateTimeKind.Utc).AddTicks(3622));

            migrationBuilder.UpdateData(
                table: "JuryContests",
                keyColumns: new[] { "JuryContestUserId", "JuryContestContestId" },
                keyValues: new object[] { 1, 2 },
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 317, DateTimeKind.Utc).AddTicks(5088));

            migrationBuilder.UpdateData(
                table: "JuryContests",
                keyColumns: new[] { "JuryContestUserId", "JuryContestContestId" },
                keyValues: new object[] { 1, 3 },
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 317, DateTimeKind.Utc).AddTicks(5106));

            migrationBuilder.UpdateData(
                table: "Phases",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(8685));

            migrationBuilder.UpdateData(
                table: "Phases",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(9827));

            migrationBuilder.UpdateData(
                table: "Phases",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(9841));

            migrationBuilder.UpdateData(
                table: "PictureReviews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 317, DateTimeKind.Utc).AddTicks(9202));

            migrationBuilder.UpdateData(
                table: "PictureReviews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 318, DateTimeKind.Utc).AddTicks(2338));

            migrationBuilder.UpdateData(
                table: "Pictures",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 317, DateTimeKind.Utc).AddTicks(354));

            migrationBuilder.UpdateData(
                table: "Pictures",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 317, DateTimeKind.Utc).AddTicks(2798));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(4244));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(5371));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(5396));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(5396));
        }
    }
}
