﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PhotoContest.Data.Migrations
{
    public partial class newmig123123 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "fe4f92de-8d11-49b3-a195-1a2f8b38d4af");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "6ce7e66f-0169-421c-a2e2-dcf85865b07d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "5be08704-30ed-4529-9cff-8f762f703512");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "cfd9cd30-21a3-4cae-ae8b-2b92a4e83c8c", "AQAAAAEAACcQAAAAEPBPxRtIU0qVaWfqstHoaCY4AzkXvJaS97AzhWJ3keDIqJoU28+vCjJVI09FfvMthQ==", "979ac41e-9a03-4f3d-8a65-cbd38ef3f0f3" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "9623d809-8971-4c5b-93ee-c5482b19dd78", "dd22e74e-b78c-4772-95bd-8a4ef48ee147" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "25f0261c-91b4-4d5d-a0a2-907349d85e06", "7f973c18-6b39-44a3-8bd6-0db35d04b3f6" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "ed7745f5-cf93-4a48-ae2a-2b3f9a21eeaa", "18801aae-f280-4a4a-a7d8-ea9d10ea7901" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "1a8b52d5-69fb-4090-90d5-1069b5748eb1", "8108e589-023a-41d8-b42e-88c02e07f038" });

            migrationBuilder.UpdateData(
                table: "ContenderContests",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 317, DateTimeKind.Utc).AddTicks(5912));

            migrationBuilder.UpdateData(
                table: "ContenderContests",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 317, DateTimeKind.Utc).AddTicks(8407));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(6596));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(7879));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(7905));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(7905));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(7905));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(7909));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 7,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(7909));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 8,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(7909));

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(2781), new DateTime(2021, 5, 20, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3331), new DateTime(2021, 5, 10, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(2091) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3911), new DateTime(2021, 5, 30, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3933), new DateTime(2021, 5, 20, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3904) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedOn", "StartDate" },
                values: new object[] { new DateTime(2021, 7, 9, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3940), new DateTime(2021, 5, 30, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3940) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3944), new DateTime(2021, 5, 20, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3944), new DateTime(2021, 5, 10, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3944) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3948), new DateTime(2021, 5, 30, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3948), new DateTime(2021, 5, 20, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3944) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedOn", "StartDate" },
                values: new object[] { new DateTime(2021, 7, 9, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3948), new DateTime(2021, 5, 30, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3948) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3951), new DateTime(2021, 5, 20, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3951), new DateTime(2021, 5, 10, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3948) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3951), new DateTime(2021, 5, 30, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3951), new DateTime(2021, 5, 20, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3951) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedOn", "StartDate" },
                values: new object[] { new DateTime(2021, 7, 9, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3951), new DateTime(2021, 5, 30, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(3951) });

            migrationBuilder.UpdateData(
                table: "ContestStates",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(4739));

            migrationBuilder.UpdateData(
                table: "ContestStates",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(5792));

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(7149));

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 4, 30, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(9607));

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 5, 30, 0, 45, 24, 316, DateTimeKind.Utc).AddTicks(9647));

            migrationBuilder.UpdateData(
                table: "JuryContests",
                keyColumns: new[] { "JuryContestUserId", "JuryContestContestId" },
                keyValues: new object[] { 1, 1 },
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 317, DateTimeKind.Utc).AddTicks(3622));

            migrationBuilder.UpdateData(
                table: "JuryContests",
                keyColumns: new[] { "JuryContestUserId", "JuryContestContestId" },
                keyValues: new object[] { 1, 2 },
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 317, DateTimeKind.Utc).AddTicks(5088));

            migrationBuilder.UpdateData(
                table: "JuryContests",
                keyColumns: new[] { "JuryContestUserId", "JuryContestContestId" },
                keyValues: new object[] { 1, 3 },
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 317, DateTimeKind.Utc).AddTicks(5106));

            migrationBuilder.UpdateData(
                table: "Phases",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(8685));

            migrationBuilder.UpdateData(
                table: "Phases",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(9827));

            migrationBuilder.UpdateData(
                table: "Phases",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(9841));

            migrationBuilder.UpdateData(
                table: "PictureReviews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 317, DateTimeKind.Utc).AddTicks(9202));

            migrationBuilder.UpdateData(
                table: "PictureReviews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 318, DateTimeKind.Utc).AddTicks(2338));

            migrationBuilder.UpdateData(
                table: "Pictures",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 317, DateTimeKind.Utc).AddTicks(354));

            migrationBuilder.UpdateData(
                table: "Pictures",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 317, DateTimeKind.Utc).AddTicks(2798));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(4244));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(5371));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(5396));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 45, 24, 315, DateTimeKind.Utc).AddTicks(5396));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "8f9bab59-7e77-4436-b98f-e08d73a6437d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "165077c3-fecc-42fc-8d67-ddd8ecb06407");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "05916533-d3fd-4129-9030-0fb5d7b77a56");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "49109d4b-5221-4860-addb-b2caf63c0d58", "AQAAAAEAACcQAAAAEK1+nqqkQ5kd1SP9Bb7qjQBrbMCzulcvPUt5ZmKEu67neg0Gc7VLl00844C7iELgBQ==", "fbe932c1-c49f-4d82-80d4-b27b37179bb2" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "afc63ef0-fa0a-4307-b177-e54b8e1be31f", "10e14111-7771-4742-981b-8bc587663da7" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "a2c080f6-f2ec-425a-947c-20a29f71ac6c", "31bfdb4d-cc87-4312-b2f3-ef22b651d186" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "f8df4217-d9ba-4ed1-876e-adbd2492ddf5", "1e75f015-3770-468d-b500-e2e756150584" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "9b8f4e2f-14b2-4079-a8f5-5a20700ca5c9", "12c28165-8a9f-4907-84c2-6d473db672a2" });

            migrationBuilder.UpdateData(
                table: "ContenderContests",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 999, DateTimeKind.Utc).AddTicks(836));

            migrationBuilder.UpdateData(
                table: "ContenderContests",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 999, DateTimeKind.Utc).AddTicks(3152));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 996, DateTimeKind.Utc).AddTicks(9751));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(951));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(977));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(977));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(977));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(977));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 7,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(977));

            migrationBuilder.UpdateData(
                table: "ContestCategories",
                keyColumn: "Id",
                keyValue: 8,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(977));

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(5706), new DateTime(2021, 5, 20, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6239), new DateTime(2021, 5, 10, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(5039) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6808), new DateTime(2021, 5, 30, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6826), new DateTime(2021, 5, 20, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6801) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedOn", "StartDate" },
                values: new object[] { new DateTime(2021, 7, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6833), new DateTime(2021, 5, 30, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6833) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6837), new DateTime(2021, 5, 20, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6837), new DateTime(2021, 5, 10, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6837) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6841), new DateTime(2021, 5, 30, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6841), new DateTime(2021, 5, 20, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6841) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedOn", "StartDate" },
                values: new object[] { new DateTime(2021, 7, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6841), new DateTime(2021, 5, 30, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6841) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6844), new DateTime(2021, 5, 20, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6844), new DateTime(2021, 5, 10, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6844) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedOn", "EndDate", "StartDate" },
                values: new object[] { new DateTime(2021, 5, 10, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6844), new DateTime(2021, 5, 30, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6844), new DateTime(2021, 5, 20, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6844) });

            migrationBuilder.UpdateData(
                table: "ContestPhases",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedOn", "StartDate" },
                values: new object[] { new DateTime(2021, 7, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6848), new DateTime(2021, 5, 30, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(6848) });

            migrationBuilder.UpdateData(
                table: "ContestStates",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(7803));

            migrationBuilder.UpdateData(
                table: "ContestStates",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(9163));

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 998, DateTimeKind.Utc).AddTicks(418));

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 4, 30, 0, 44, 44, 998, DateTimeKind.Utc).AddTicks(2839));

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 5, 30, 0, 44, 44, 998, DateTimeKind.Utc).AddTicks(2872));

            migrationBuilder.UpdateData(
                table: "JuryContests",
                keyColumns: new[] { "JuryContestUserId", "JuryContestContestId" },
                keyValues: new object[] { 1, 1 },
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 998, DateTimeKind.Utc).AddTicks(6708));

            migrationBuilder.UpdateData(
                table: "JuryContests",
                keyColumns: new[] { "JuryContestUserId", "JuryContestContestId" },
                keyValues: new object[] { 1, 2 },
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 998, DateTimeKind.Utc).AddTicks(9873));

            migrationBuilder.UpdateData(
                table: "JuryContests",
                keyColumns: new[] { "JuryContestUserId", "JuryContestContestId" },
                keyValues: new object[] { 1, 3 },
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 998, DateTimeKind.Utc).AddTicks(9895));

            migrationBuilder.UpdateData(
                table: "Phases",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(1856));

            migrationBuilder.UpdateData(
                table: "Phases",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(2855));

            migrationBuilder.UpdateData(
                table: "Phases",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 997, DateTimeKind.Utc).AddTicks(2873));

            migrationBuilder.UpdateData(
                table: "PictureReviews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 999, DateTimeKind.Utc).AddTicks(3921));

            migrationBuilder.UpdateData(
                table: "PictureReviews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 999, DateTimeKind.Utc).AddTicks(7068));

            migrationBuilder.UpdateData(
                table: "Pictures",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 998, DateTimeKind.Utc).AddTicks(3565));

            migrationBuilder.UpdateData(
                table: "Pictures",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 998, DateTimeKind.Utc).AddTicks(5953));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 996, DateTimeKind.Utc).AddTicks(7578));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 996, DateTimeKind.Utc).AddTicks(8851));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 996, DateTimeKind.Utc).AddTicks(8876));

            migrationBuilder.UpdateData(
                table: "Ranks",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2021, 6, 9, 0, 44, 44, 996, DateTimeKind.Utc).AddTicks(8876));
        }
    }
}
