﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PhotoContest.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ContestCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContestCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ContestStates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContestStates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Phases",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Phases", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Ranks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ranks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Contests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    ContestCategoryPhaseDescription = table.Column<string>(nullable: false),
                    Foreground_Url = table.Column<string>(nullable: true),
                    ContestCategoryId = table.Column<int>(nullable: false),
                    ContestStateId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Contests_ContestCategories_ContestCategoryId",
                        column: x => x.ContestCategoryId,
                        principalTable: "ContestCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Contests_ContestStates_ContestStateId",
                        column: x => x.ContestStateId,
                        principalTable: "ContestStates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(maxLength: 21, nullable: false),
                    LastName = table.Column<string>(maxLength: 20, nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Points = table.Column<long>(nullable: false),
                    RankId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_Ranks_RankId",
                        column: x => x.RankId,
                        principalTable: "Ranks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContestPhases",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    PhaseId = table.Column<int>(nullable: false),
                    ContestId = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContestPhases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContestPhases_Contests_ContestId",
                        column: x => x.ContestId,
                        principalTable: "Contests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContestPhases_Phases_PhaseId",
                        column: x => x.PhaseId,
                        principalTable: "Phases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Pictures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 30, nullable: false),
                    Story = table.Column<string>(maxLength: 3000, nullable: true),
                    Url = table.Column<string>(nullable: false),
                    ContestId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pictures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Pictures_Contests_ContestId",
                        column: x => x.ContestId,
                        principalTable: "Contests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderKey = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    Name = table.Column<string>(maxLength: 128, nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "JuryContests",
                columns: table => new
                {
                    JuryContestUserId = table.Column<int>(nullable: false),
                    JuryContestContestId = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Id = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: true),
                    ContestId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JuryContests", x => new { x.JuryContestUserId, x.JuryContestContestId });
                    table.ForeignKey(
                        name: "FK_JuryContests_Contests_ContestId",
                        column: x => x.ContestId,
                        principalTable: "Contests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_JuryContests_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContenderContests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    ContestId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    PictureId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContenderContests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContenderContests_Contests_ContestId",
                        column: x => x.ContestId,
                        principalTable: "Contests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContenderContests_Pictures_PictureId",
                        column: x => x.PictureId,
                        principalTable: "Pictures",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ContenderContests_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PictureReviews",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Score = table.Column<double>(nullable: false),
                    Comment = table.Column<string>(nullable: false),
                    DoesntFitsCategoryCheckBox = table.Column<bool>(nullable: false),
                    JuryContestContestId = table.Column<int>(nullable: false),
                    JuryContestUserId = table.Column<int>(nullable: true),
                    JuryContestContestId1 = table.Column<int>(nullable: true),
                    PictureId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PictureReviews", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PictureReviews_Pictures_PictureId",
                        column: x => x.PictureId,
                        principalTable: "Pictures",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PictureReviews_JuryContests_JuryContestUserId_JuryContestContestId1",
                        columns: x => new { x.JuryContestUserId, x.JuryContestContestId1 },
                        principalTable: "JuryContests",
                        principalColumns: new[] { "JuryContestUserId", "JuryContestContestId" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 3, "8490f96d-c83c-4410-8211-548645869fd0", "Organizer", "ORGANIZER" },
                    { 2, "c73b4446-5a3c-46c0-b3b3-ec61cd30addb", "User", "USER" },
                    { 1, "aabe1034-ab4a-40ae-b2b2-dd0da95db30b", "Admin", "ADMIN" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "CreatedOn", "DeletedOn", "Email", "EmailConfirmed", "FirstName", "IsDeleted", "LastName", "LockoutEnabled", "LockoutEnd", "ModifiedOn", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "Points", "RankId", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { 5, 0, "aa8e24b4-f30f-4e13-9287-86a152903551", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "givanov@abv.bg", true, "Georgi", false, "Ivanov", false, null, null, "GIVANOV@ABV.BG", "GEORGIIVANOV@ABV.BG", null, null, false, 0L, null, "60013566-d945-4893-8158-a1ae3e9c4bf4", false, "georgiivanov@abv.bg" },
                    { 3, 0, "be6402d2-3640-4b02-9919-eb7611d6e66c", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "gpenev@abv.bg", true, "Georgi", false, "Penev", false, null, null, "GPENEV@ABV.BG", "GEORGIPENEV@ABV.BG", null, null, false, 0L, null, "511edddf-bcac-40ab-85d4-398a95ccf31d", false, "georhipenev@abv.bg" },
                    { 2, 0, "9f3e05c8-1a36-40ef-b582-f5f7f1eefa09", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "sivanov@abv.bg", true, "Stivan", false, "Ivanov", false, null, null, "SIVANOV@ABV.BG", "STIVANIVANOV@ABV.BG", null, null, false, 0L, null, "6553ce53-d975-45a4-9e78-a912f5e683d9", false, "stivanivanov@abv.bg" },
                    { 1, 0, "73efedb1-53d8-47ee-af1d-68ae5d8d804b", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "AAdminov@mail.bg", false, "Admin", false, "Adminov", true, null, null, "AADMINOV@mail.bg", "AADMINOV@MAIL.BG", "AQAAAAEAACcQAAAAEKv2kxoJPK/0989TqjhJGd3ykdv8VmzApIuj4sPQcm8IH36lpkXj+gdt4x8xJz0Mbw==", null, false, 0L, null, "f5da46ec-1573-4131-8fd9-7533a09b6f6d", false, "AdminAdminov@mail.bg" },
                    { 4, 0, "adacc750-157e-4c0d-a80b-47dd7f663657", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "iivanov@abv.bg", true, "Ivan", false, "Ivanov", false, null, null, "IIVANOV@ABV.BG", "IVANIVANOV@ABV.BG", null, null, false, 0L, null, "794659fd-65a1-490e-84f1-40aff12f8122", false, "ivanivanov@abv.bg" }
                });

            migrationBuilder.InsertData(
                table: "ContestCategories",
                columns: new[] { "Id", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(1130), null, false, null, "Portrait Photography" },
                    { 8, new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(2737), null, false, null, "Adventure Photography" },
                    { 2, new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(2645), null, false, null, "Fashion Photography" },
                    { 3, new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(2724), null, false, null, "Sports Photography" },
                    { 4, new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(2729), null, false, null, "Photojournalism" },
                    { 5, new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(2731), null, false, null, "Editorial Photography" },
                    { 6, new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(2733), null, false, null, "Architectural Photography" },
                    { 7, new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(2735), null, false, null, "Food Photography" }
                });

            migrationBuilder.InsertData(
                table: "ContestStates",
                columns: new[] { "Id", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Name" },
                values: new object[,]
                {
                    { 2, new DateTime(2021, 6, 8, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(4602), null, false, null, "Invitational" },
                    { 1, new DateTime(2021, 6, 8, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(2950), null, false, null, "Open" }
                });

            migrationBuilder.InsertData(
                table: "JuryContests",
                columns: new[] { "JuryContestUserId", "JuryContestContestId", "ContestId", "CreatedOn", "DeletedOn", "Id", "IsDeleted", "ModifiedOn", "UserId" },
                values: new object[,]
                {
                    { 1, 1, null, new DateTime(2021, 6, 8, 18, 52, 58, 40, DateTimeKind.Utc).AddTicks(4824), null, 1, false, null, null },
                    { 1, 2, null, new DateTime(2021, 6, 8, 18, 52, 58, 40, DateTimeKind.Utc).AddTicks(6366), null, 2, false, null, null },
                    { 1, 3, null, new DateTime(2021, 6, 8, 18, 52, 58, 40, DateTimeKind.Utc).AddTicks(6414), null, 3, false, null, null }
                });

            migrationBuilder.InsertData(
                table: "Phases",
                columns: new[] { "Id", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(4086), null, false, null, "PhaseI" },
                    { 3, new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(5511), null, false, null, "Finished" },
                    { 2, new DateTime(2021, 6, 8, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(5444), null, false, null, "PhaseII" }
                });

            migrationBuilder.InsertData(
                table: "Ranks",
                columns: new[] { "Id", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Name" },
                values: new object[,]
                {
                    { 2, new DateTime(2021, 6, 8, 18, 52, 58, 37, DateTimeKind.Utc).AddTicks(9916), null, false, null, "Master" },
                    { 1, new DateTime(2021, 6, 8, 18, 52, 58, 37, DateTimeKind.Utc).AddTicks(8675), null, false, null, "Junkie" },
                    { 4, new DateTime(2021, 6, 8, 18, 52, 58, 37, DateTimeKind.Utc).AddTicks(9980), null, false, null, "Wise and Benevolent Photo Dictator" },
                    { 3, new DateTime(2021, 6, 8, 18, 52, 58, 37, DateTimeKind.Utc).AddTicks(9978), null, false, null, "Enthusiast" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 1, 3 }
                });

            migrationBuilder.InsertData(
                table: "Contests",
                columns: new[] { "Id", "ContestCategoryId", "ContestCategoryPhaseDescription", "ContestStateId", "CreatedOn", "DeletedOn", "Foreground_Url", "IsDeleted", "ModifiedOn", "Title" },
                values: new object[,]
                {
                    { 1, 1, "Phase Two", 1, new DateTime(2021, 6, 8, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(6747), null, "https://images.unsplash.com/photo-1522335789203-aabd1fc54bc9?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1065&q=80", false, null, "Fashion Faces" },
                    { 2, 1, "Phase Two", 1, new DateTime(2021, 4, 29, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(9851), null, "https://images.unsplash.com/photo-1522335789203-aabd1fc54bc9?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1065&q=80", false, null, "Fashion Faces" },
                    { 3, 1, "Finished", 1, new DateTime(2021, 5, 29, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(9914), null, "https://images.unsplash.com/photo-1522335789203-aabd1fc54bc9?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1065&q=80", false, null, "Fashion Faces" }
                });

            migrationBuilder.InsertData(
                table: "ContestPhases",
                columns: new[] { "Id", "ContestId", "CreatedOn", "DeletedOn", "EndDate", "IsDeleted", "ModifiedOn", "PhaseId", "StartDate" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2021, 5, 9, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(9787), null, new DateTime(2021, 5, 19, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(526), false, null, 1, new DateTime(2021, 5, 9, 18, 52, 58, 38, DateTimeKind.Utc).AddTicks(8645) },
                    { 2, 1, new DateTime(2021, 5, 9, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1324), null, new DateTime(2021, 5, 29, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1386), false, null, 2, new DateTime(2021, 5, 19, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1297) },
                    { 3, 1, new DateTime(2021, 7, 8, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1405), null, new DateTime(9999, 12, 31, 23, 59, 59, 999, DateTimeKind.Unspecified).AddTicks(9999), false, null, 2, new DateTime(2021, 5, 29, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1403) },
                    { 4, 2, new DateTime(2021, 5, 9, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1473), null, new DateTime(2021, 5, 19, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1476), false, null, 1, new DateTime(2021, 5, 9, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1409) },
                    { 5, 2, new DateTime(2021, 5, 9, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1482), null, new DateTime(2021, 5, 29, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1484), false, null, 2, new DateTime(2021, 5, 19, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1481) },
                    { 6, 2, new DateTime(2021, 7, 8, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1489), null, new DateTime(9999, 12, 31, 23, 59, 59, 999, DateTimeKind.Unspecified).AddTicks(9999), false, null, 3, new DateTime(2021, 5, 29, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1488) },
                    { 7, 3, new DateTime(2021, 5, 9, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1501), null, new DateTime(2021, 5, 19, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1503), false, null, 1, new DateTime(2021, 5, 9, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1499) },
                    { 8, 3, new DateTime(2021, 5, 9, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1508), null, new DateTime(2021, 5, 29, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1509), false, null, 2, new DateTime(2021, 5, 19, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1506) },
                    { 9, 3, new DateTime(2021, 7, 8, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1514), null, new DateTime(9999, 12, 31, 23, 59, 59, 999, DateTimeKind.Unspecified).AddTicks(9999), false, null, 3, new DateTime(2021, 5, 29, 18, 52, 58, 39, DateTimeKind.Utc).AddTicks(1512) }
                });

            migrationBuilder.InsertData(
                table: "Pictures",
                columns: new[] { "Id", "ContestId", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Name", "Story", "Url" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2021, 6, 8, 18, 52, 58, 40, DateTimeKind.Utc).AddTicks(991), null, false, null, "Beutifull women", "Extravagant dressed women", "https://www.jdinstitute.edu.in/media/2019/03/Fashion-Photography-1.jpg" },
                    { 2, 1, new DateTime(2021, 6, 8, 18, 52, 58, 40, DateTimeKind.Utc).AddTicks(3763), null, false, null, "Hidden ice photo", "wommed covering her eyse ", "https://expertphotography.com/wp-content/uploads/2019/04/fashion-photography-inspiration-6.jpg" }
                });

            migrationBuilder.InsertData(
                table: "ContenderContests",
                columns: new[] { "Id", "ContestId", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "PictureId", "UserId" },
                values: new object[] { 1, 1, new DateTime(2021, 6, 8, 18, 52, 58, 40, DateTimeKind.Utc).AddTicks(7452), null, false, null, 1, 2 });

            migrationBuilder.InsertData(
                table: "PictureReviews",
                columns: new[] { "Id", "Comment", "CreatedOn", "DeletedOn", "DoesntFitsCategoryCheckBox", "IsDeleted", "JuryContestContestId", "JuryContestContestId1", "JuryContestUserId", "ModifiedOn", "PictureId", "Score" },
                values: new object[] { 1, "great work", new DateTime(2021, 6, 8, 18, 52, 58, 41, DateTimeKind.Utc).AddTicks(760), null, false, false, 3, null, null, null, 1, 5.0 });

            migrationBuilder.InsertData(
                table: "PictureReviews",
                columns: new[] { "Id", "Comment", "CreatedOn", "DeletedOn", "DoesntFitsCategoryCheckBox", "IsDeleted", "JuryContestContestId", "JuryContestContestId1", "JuryContestUserId", "ModifiedOn", "PictureId", "Score" },
                values: new object[] { 3, "great work", new DateTime(2021, 6, 8, 18, 52, 58, 41, DateTimeKind.Utc).AddTicks(6673), null, false, false, 2, null, null, null, 2, 8.0 });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_RankId",
                table: "AspNetUsers",
                column: "RankId");

            migrationBuilder.CreateIndex(
                name: "IX_ContenderContests_ContestId",
                table: "ContenderContests",
                column: "ContestId");

            migrationBuilder.CreateIndex(
                name: "IX_ContenderContests_PictureId",
                table: "ContenderContests",
                column: "PictureId",
                unique: true,
                filter: "[PictureId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ContenderContests_UserId",
                table: "ContenderContests",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ContestPhases_ContestId",
                table: "ContestPhases",
                column: "ContestId");

            migrationBuilder.CreateIndex(
                name: "IX_ContestPhases_PhaseId",
                table: "ContestPhases",
                column: "PhaseId");

            migrationBuilder.CreateIndex(
                name: "IX_Contests_ContestCategoryId",
                table: "Contests",
                column: "ContestCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Contests_ContestStateId",
                table: "Contests",
                column: "ContestStateId");

            migrationBuilder.CreateIndex(
                name: "IX_JuryContests_ContestId",
                table: "JuryContests",
                column: "ContestId");

            migrationBuilder.CreateIndex(
                name: "IX_JuryContests_UserId",
                table: "JuryContests",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_PictureReviews_PictureId",
                table: "PictureReviews",
                column: "PictureId");

            migrationBuilder.CreateIndex(
                name: "IX_PictureReviews_JuryContestUserId_JuryContestContestId1",
                table: "PictureReviews",
                columns: new[] { "JuryContestUserId", "JuryContestContestId1" });

            migrationBuilder.CreateIndex(
                name: "IX_Pictures_ContestId",
                table: "Pictures",
                column: "ContestId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "ContenderContests");

            migrationBuilder.DropTable(
                name: "ContestPhases");

            migrationBuilder.DropTable(
                name: "PictureReviews");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Phases");

            migrationBuilder.DropTable(
                name: "Pictures");

            migrationBuilder.DropTable(
                name: "JuryContests");

            migrationBuilder.DropTable(
                name: "Contests");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "ContestCategories");

            migrationBuilder.DropTable(
                name: "ContestStates");

            migrationBuilder.DropTable(
                name: "Ranks");
        }
    }
}
