﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Seeder
{
    public static class ModelBuilderExtention
    {

        public static void Seeder(this ModelBuilder builder)
        {  //seeding roles
            builder.Entity<Role>().HasData(
                new Role { Id = 1, Name = "Admin", NormalizedName = "ADMIN" },
                new Role { Id = 2, Name = "User", NormalizedName = "USER" },
                new Role { Id = 3, Name = "Organizer", NormalizedName = "ORGANIZER" }

                );
            //seeding manager account
            var hasher = new PasswordHasher<User>();
            User managerUserr = new User
            {
                Id = 1,
                FirstName = "Admin",
                LastName = "Adminov",
                UserName = "AdminAdminov@mail.bg",
                NormalizedUserName = "AADMINOV@MAIL.BG",
                Email = "AAdminov@mail.bg",
                NormalizedEmail = "AADMINOV@mail.bg",
                LockoutEnabled = true,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            managerUserr.PasswordHash = hasher.HashPassword(managerUserr, "Parola");

            builder.Entity<User>().HasData(managerUserr);

            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>
                {
                    RoleId = 1,
                    UserId = 1

                },
                new IdentityUserRole<int>
                {
                    RoleId = 3,
                    UserId = 1,

                });
            //seeding users
            var user1 = new User
            {
                Id = 2,
                FirstName = "Stivan",
                LastName = "Ivanov",
                UserName = "stivanivanov@abv.bg",
                NormalizedUserName = "stivanivanov@abv.bg".ToUpper(),
                Email = "sivanov@abv.bg",
                NormalizedEmail = "sivanov@abv.bg".ToUpper(),
                EmailConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString(),
                Points = 0
                



            };
            var user2 = new User
            {
                Id = 3,
                FirstName = "Georgi",
                LastName = "Penev",
                UserName = "georhipenev@abv.bg",
                NormalizedUserName = "georgipenev@abv.bg".ToUpper(),
                Email = "gpenev@abv.bg",
                NormalizedEmail = "gpenev@abv.bg".ToUpper(),
                EmailConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString(),
                Points = 0



            }; var user3 = new User
            {
                Id = 4,
                FirstName = "Ivan",
                LastName = "Ivanov",
                UserName = "ivanivanov@abv.bg",
                NormalizedUserName = "ivanivanov@abv.bg".ToUpper(),
                Email = "iivanov@abv.bg",
                NormalizedEmail = "iivanov@abv.bg".ToUpper(),
                EmailConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString(),
                Points = 0



            }; var user4 = new User
            {
                Id = 5,
                FirstName = "Georgi",
                LastName = "Ivanov",
                UserName = "georgiivanov@abv.bg",
                NormalizedUserName = "georgiivanov@abv.bg".ToUpper(),
                Email = "givanov@abv.bg",
                NormalizedEmail = "givanov@abv.bg".ToUpper(),
                EmailConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString(),
                Points = 0,



            };

            builder.Entity<User>().HasData(user1, user2, user3, user4);

            builder.Entity<Rank>().HasData(
               new Rank[] {
                        new Rank(){Id = 1,Name = "Junkie"},
                        new Rank(){Id = 2,Name = "Master"},
                        new Rank(){Id = 3,Name = "Enthusiast"},
                        new Rank(){Id = 4,Name = "Wise and Benevolent Photo Dictator"}


               });



            builder.Entity<ContestCategory>().HasData(
               new ContestCategory[] {
                        new ContestCategory(){Id = 1,Name = "Portrait Photography"},
                        new ContestCategory(){Id = 2,Name = "Fashion Photography"},
                        new ContestCategory(){Id = 3,Name = "Sports Photography"},
                        new ContestCategory(){Id = 4,Name = "Photojournalism"},
                        new ContestCategory(){Id = 5,Name = "Editorial Photography"},
                        new ContestCategory(){Id = 6,Name = "Architectural Photography"},
                        new ContestCategory(){Id = 7,Name = "Food Photography"},
                        new ContestCategory(){Id = 8,Name = "Adventure Photography"},

               });
            /*builder.Entity<ContenderContest>().HasData(
              new ContenderContest[] {
                        new ContenderContest(){Id = 1, ContestId=1, UserId = 1,PictureId=1},
                        new ContenderContest(){Id = 2, ContestId=1, UserId = 2,PictureId=1},
              });*/
            builder.Entity<Phase>().HasData(
              new Phase[] {
                        new Phase(){Id = 1,Name="PhaseI"},
                        new Phase(){Id = 2,Name="PhaseII"},
                        new Phase(){Id = 3,Name="Finished"},


              });

            builder.Entity<ContestPhase>().HasData(
               new ContestPhase[]
               {
                   // ONE
                        new ContestPhase()
                        {   Id = 1,
                            ContestId = 1,
                            PhaseId=1,
                            StartDate =DateTime.UtcNow.AddDays(-1),
                            CreatedOn=DateTime.UtcNow.AddDays(-1),
                            EndDate=DateTime.UtcNow.AddHours(-1)

                        },
                        new ContestPhase()
                        {
                            Id = 2,
                            ContestId = 1,
                            PhaseId=2,
                            StartDate =DateTime.UtcNow.AddHours(-1),
                            CreatedOn=DateTime.UtcNow.AddDays(-30),
                            EndDate=DateTime.UtcNow.AddHours(24)

                        },
                       new ContestPhase()
                       {
                            Id = 3,
                            ContestId = 1,
                            PhaseId=2,
                            StartDate =DateTime.UtcNow.AddHours(24),
                            CreatedOn=DateTime.UtcNow.AddDays(30),
                            EndDate=DateTime.MaxValue

                       },
                       //TWO
                        new ContestPhase()
                        {
                            Id = 4,
                            ContestId = 2,
                            PhaseId=1,
                            StartDate =DateTime.UtcNow.AddDays(-30),
                            CreatedOn=DateTime.UtcNow.AddDays(-30),
                            EndDate=DateTime.UtcNow.AddDays(-20)

                        },
                        new ContestPhase()
                        {
                            Id = 5,
                            ContestId = 2,
                            PhaseId=2,
                            StartDate =DateTime.UtcNow.AddDays(-20),
                            CreatedOn=DateTime.UtcNow.AddDays(-30),
                            EndDate=DateTime.UtcNow.AddDays(-10)

                        },
                       new ContestPhase()
                       {
                           Id = 6,
                            ContestId = 2,
                            PhaseId=3,
                            StartDate =DateTime.UtcNow.AddDays(-10),
                            CreatedOn=DateTime.UtcNow.AddDays(30),
                            EndDate=DateTime.MaxValue

                       },
                       // FINISHED

                        new ContestPhase()
                        {
                            Id = 7,
                            ContestId = 3,
                            PhaseId=1,
                            StartDate =DateTime.UtcNow.AddDays(-30),
                            CreatedOn=DateTime.UtcNow.AddDays(-30),
                            EndDate=DateTime.UtcNow.AddDays(-20)

                        },
                        new ContestPhase()
                        {
                            Id = 8,
                            ContestId = 3,
                            PhaseId=2,
                            StartDate =DateTime.UtcNow.AddDays(-20),
                            CreatedOn=DateTime.UtcNow.AddDays(-30),
                            EndDate=DateTime.UtcNow.AddDays(-10)

                        },
                       new ContestPhase()
                       {
                            Id=9,
                            ContestId = 3,
                            PhaseId=3,
                            StartDate =DateTime.UtcNow.AddDays(-10),
                            CreatedOn=DateTime.UtcNow.AddDays(30),
                            EndDate=DateTime.MaxValue
                       },

               });

            builder.Entity<ContestState>().HasData(
             new ContestState[] {
                        new ContestState(){Id = 1,Name="Open"},
                        new ContestState(){Id = 2,Name="Invitational"},

             });
            builder.Entity<Contest>().HasData(
               new Contest[]
               {
                        new Contest()
                        {
                             Id = 1,
                             CreatedOn=DateTime.UtcNow,
                             ContestCategoryId=1,
                             ContestStateId=1,
                             Title = "Fashion Faces",
                             ContestCategoryPhaseDescription="Phase Two",
                             Foreground_Url="https://images.unsplash.com/photo-1522335789203-aabd1fc54bc9?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1065&q=80"
                        },

                        new Contest()
                        {
                             Id = 2,
                             CreatedOn=DateTime.UtcNow.AddDays(-40),
                             ContestCategoryId=1,
                             ContestStateId=1,
                             Title = "Lightning Contest",
                             ContestCategoryPhaseDescription="Finished",
                             Foreground_Url="https://www.ecmwf.int/sites/default/files/styles/news_item_main_image/public/ThinkstockPhotos-Lightning-146914924-690px.jpg?itok=BClSyctU"
                        },

                        new Contest()
                        {
                             Id = 3,
                             CreatedOn=DateTime.UtcNow.AddDays(-10),
                             ContestCategoryId=1,
                             ContestStateId=1,
                             Title = "Benji The Bird",
                             ContestCategoryPhaseDescription="Finished",
                             Foreground_Url="http://www.natureimagesawards.com/wp-content/uploads/2019/05/humming-bird.jpg"
                        },



               });

            builder.Entity<Picture>().HasData(
               new Picture[] {
                        new Picture ()
                        {
                                Id=1,
                                ContestId = 1,
                                Name = "Beautiful woman",
                                Story = "Extravagantly dressed woman.",
                                Url = "https://www.jdinstitute.edu.in/media/2019/03/Fashion-Photography-1.jpg"
                        },

                        new Picture ()
                        {
                                Id=2,
                                ContestId = 1,
                                Name = "Hidden ice photo",
                                Story = "A womman covering her eyes.",
                                Url = "https://expertphotography.com/wp-content/uploads/2019/04/fashion-photography-inspiration-6.jpg"
                        },

                         new Picture ()
                        {
                                Id=3,
                                ContestId = 1,
                                Name = "Milky and volcano.",
                                Story = "Volcano under milky way",
                                Url = "https://images.unsplash.com/photo-1536725527136-e5f9631216d1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=381&q=80"
                        },



               });

            builder.Entity<JuryContest>().HasData(
               new JuryContest[] {
                        new JuryContest(){Id=1,JuryContestContestId=1,JuryContestUserId=1},
                        new JuryContest(){Id=2,JuryContestContestId=2,JuryContestUserId=1},
                        new JuryContest(){Id=3,JuryContestContestId=3,JuryContestUserId=1},
               });

            builder.Entity<ContenderContest>().HasData(
               new[] {
                      //  new ContenderContest(){Id=1,UserId=1,PictureId=1,ContestId=1},
                        new ContenderContest(){Id = 1, ContestId=1, UserId = 2,PictureId=1},
                        new ContenderContest(){Id = 2, ContestId=1, UserId = 3,PictureId=2},
               });

            builder.Entity<PictureReview>().HasData(
               new PictureReview[]
               {
                   new PictureReview()
                   {
                       Id=1,
                       PictureId=1,
                       JuryContestContestId=3,
                       DoesntFitsCategoryCheckBox=false,
                       Comment="great work",
                       Score=5,

                   },

                   new PictureReview()
                   {
                       Id=3,
                       PictureId=2,
                       JuryContestContestId=2,
                       DoesntFitsCategoryCheckBox=false,
                       Comment="great work",
                       Score=8,


                   }
               });




        }


    }
}
