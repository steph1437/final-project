﻿using Microsoft.AspNetCore.Identity;
using PhotoContest.Data.Entities.Abstracts;
using PhotoContest.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoContest.Data
{
    public class User : IdentityUser<int>, IEntity
    {
        [Key]
        public override int Id { get; set; }

        [Required]
        [MinLength(1), MaxLength(21)]
        public string FirstName { get; set; }

        [Required]
        [MinLength(1), MaxLength(20)]
        public string LastName { get; set; }

        [Required]
        [MinLength(4), MaxLength(124)]
        public override string UserName { get => base.UserName; set => base.UserName = value; }

        public DateTime CreatedOn { get ; set ; }
        public DateTime? ModifiedOn { get ; set; }
        public DateTime? DeletedOn { get ; set ; }
        public bool IsDeleted { get ; set ; }

        public Int64 Points { get; set; }

        public ICollection<JuryContest> JuryContests { get; set; }

        public ICollection<ContenderContest> ContenderContests  { get; set; } 
    }
}
