﻿using PhotoContest.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoContest.Data.Models
{
    public class Phase : Entity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(maximumLength: 20)]
        public string Name { get; set; }

        public ICollection<ContestPhase> Contests { get; set; }
    }
}
