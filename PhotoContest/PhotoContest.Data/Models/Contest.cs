﻿using PhotoContest.Data.Entities;
using PhotoContest.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoContest.Data
{
    public class Contest : Entity
    {
            [Key]
            public int Id { get; set; }

            [Required]
            [MinLength(5), MaxLength(50)]
            public string Title { get; set; }
            [Required]
            public string ContestCategoryPhaseDescription { get; set; }


            public string Foreground_Url { get; set; }
       
            public int ContestCategoryId { get; set; }
            public ContestCategory ContestCategory { get; set; }

            public int ContestStateId { get; set; }
            public ContestState ContestState { get; set; }


            public ICollection<Picture> Pictures { get; set; }

            public ICollection<JuryContest> UserContests { get; set; }

            public ICollection<ContenderContest> ContenderContests { get; set; }

            public ICollection<ContestPhase> ContestPhases { get; set; }
    }
}
