﻿using PhotoContest.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoContest.Data
{
    public class JuryContest:Entity
    {

        [Key]
        public int Id { get; set; }

        public int JuryContestUserId { get; set; }
        public User User { get; set; }

        public int JuryContestContestId { get; set; }
        public Contest Contest { get; set; }

        public ICollection<PictureReview> PictureReviews { get; set; }
    }
}
