﻿using PhotoContest.Data.Entities;
using PhotoContest.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoContest.Data
{
    public class ContestPhase : Entity
    {
        [Key]
        public int Id { get; set; }

        public int PhaseId { get; set; }
        public Phase Phase { get; set; }
        
        public int ContestId { get; set; }
        public Contest Contest { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
