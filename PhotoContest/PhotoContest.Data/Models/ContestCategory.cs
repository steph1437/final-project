﻿using PhotoContest.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoContest.Data
{
    public class ContestCategory:Entity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(maximumLength:30)]
        public string Name { get; set; }

        public ICollection<Contest> Contests { get; set; }
    }
}
