﻿using PhotoContest.Data.Entities;
using PhotoContest.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoContest.Data
{
    public class Picture : Entity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(maximumLength: 30)]
        public string Name { get; set; }

        [StringLength(maximumLength: 3000)]
        public string Story { get; set; }

        [Required]
        public string Url { get; set; }

        public int ContestId { get; set; }
        public Contest Contest { get; set; }

        public ICollection<PictureReview> PictureReviews {get; set;}

        public ContenderContest ContenderContest {get; set;}

        

    }
}
