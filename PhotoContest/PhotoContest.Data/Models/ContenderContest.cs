﻿using PhotoContest.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoContest.Data.Models
{
    public class ContenderContest : Entity
    {
        [Key]
        public int Id { get; set; }

        public int ContestId { get; set; }
        public Contest Contest { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        public int? PictureId { get; set; }
        public Picture Picture { get; set; }
    }
}
