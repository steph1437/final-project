﻿using PhotoContest.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoContest.Data
{
    public class PictureReview : Entity
    {
        [Key]
        public int Id { get; set; }

        [Range(0, 10)]
        public double Score { get; set; }

        [Required]
        public string Comment { get; set; }

        public bool DoesntFitsCategoryCheckBox { get; set; }

        public int JuryContestContestId { get; set; }
        public JuryContest JuryContest { get; set; }

        public int PictureId { get; set; }
        public Picture Picture { get; set; }



    }
}
