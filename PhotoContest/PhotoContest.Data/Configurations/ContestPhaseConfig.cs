﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Configurations
{
    public class ContestPhaseConfig:IEntityTypeConfiguration<ContestPhase>
    {
        public void Configure(EntityTypeBuilder<ContestPhase> builder)
        {
            builder.HasKey(contestphase => new { contestphase.ContestId, contestphase.PhaseId });
        }
    }
}
