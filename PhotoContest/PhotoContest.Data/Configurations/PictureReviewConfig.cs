﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Configurations
{
    public class PictureReviewConfig : IEntityTypeConfiguration<PictureReview>
    {
        public void Configure(EntityTypeBuilder<PictureReview> builder)
        {
            builder.HasKey(pr => pr.Id);
            builder.HasIndex(picturereview => new { picturereview.PictureId, picturereview.JuryContestContestId }).IsUnique();
        }
    }
}
