﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Configurations
{
    public class JuryContestConfig : IEntityTypeConfiguration<JuryContest>
    {
        public void Configure(EntityTypeBuilder<JuryContest> builder)
        {
            builder.HasKey(jc => new { jc.JuryContestContestId, jc.JuryContestUserId });
            builder.HasIndex(jc => new { jc.JuryContestContestId, jc.JuryContestUserId }).IsUnique();


        }
    }
}
