﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Configurations
{
    public class ContenderContestConfig:IEntityTypeConfiguration<JuryContest>
    {
        public void Configure(EntityTypeBuilder<JuryContest> builder)
        {
            builder
                .HasKey(userContest=>new
                {
                    userContest.JuryContestUserId,
                    userContest.JuryContestContestId
                });
        }
    }
}
