﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data.Configurations;
using PhotoContest.Data.Models;
using PhotoContest.Data.Seeder;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Context
{
    public class PhotoContestContext : IdentityDbContext<User, Role, int>
    {
        public PhotoContestContext(DbContextOptions<PhotoContestContext> options)
         : base(options)
        {

        }

        public DbSet<Contest> Contests { get; set; }
        public DbSet<ContestCategory> ContestCategories { get; set; }
        public DbSet<ContestPhase> ContestPhases { get; set; }
        public DbSet<ContestState> ContestStates { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<PictureReview> PictureReviews { get; set; }
        public DbSet<ContenderContest> ContenderContests { get; set; }
        public DbSet<JuryContest> JuryContests { get; set; }
        public DbSet<Phase> Phases { get; set; }
        public DbSet<Rank> Ranks { get; set; }




        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.ApplyConfiguration(new PictureConfig());
            modelBuilder.ApplyConfiguration(new ContenderContestConfig());
            modelBuilder.Seeder();

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
    }
}
