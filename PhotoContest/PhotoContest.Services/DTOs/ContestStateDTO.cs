﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Services.DTOs
{
    public class ContestStateDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<ContestDTO> Contests { get; set; }
    }
}
