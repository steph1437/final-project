﻿using PhotoContest.Services.Providers;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Services.DTOs
{
    public class ContestInputDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public string Foreground_Url { get; set; }

        public string ContestCategoryPhaseDescription { get; set; }

        public PhaseProvider Phases { get; set; }

        public int ContestCategoryId { get; set; }

        public int ContestStateId { get; set; }
    }
}
