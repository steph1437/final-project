﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Services.DTOs
{
    public class ContestOutputFrontDTO
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string ContestCategoryPhaseDescription { get; set; }

        public string Foreground_Url { get; set; }

        public int PhaseId { get; set; }

        public ContestPhasesDTO GetActivePhase
        {
            get
            {
                foreach (var phase in PhasesInformation)
                {
                    if (phase.IsActive)
                    {
                        return phase;
                    }
                }

                return null;
            }
        }

        public ICollection<ContestPhasesDTO> PhasesInformation { get; set; }

        public int ContestCategoryId { get; set; }

        public int ContestStateId { get; set; }
    }
}
