﻿using PhotoContest.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Services.DTOs
{
    public class ContestDTO
    {

        public int Id { get; set; }


        public string Title { get; set; }

        public string ContestCategoryPhaseDescription { get; set; }
        public string Foreground_Url { get; set; }

        public ContestPhase Phase { get; set; }
        public int ContestCategoryId { get; set; }
        public ContestCategoryDTO ContestCategory { get; set; }

        public int ContestStateId { get; set; }
        public ContestStateDTO ContestState { get; set; }


        public ICollection<PictureDTO> Pictures { get; set; }

        public ICollection<JuryContestDTO> UserContests { get; set; }

        public ICollection<ContenderContestDTO> ContenderContests { get; set; }


    }
}
