﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Services.DTOs
{
    public class PictureReviewDTO
    {
        public int Id { get; set; }

        public double Score { get; set; }

        public string Comment { get; set; }

        public bool DoesntFitsCategoryCheckBox { get; set; }

        public int JuryContestContestId { get; set; }
        public JuryContestDTO JuryContest { get; set; }

        public int PictureId { get; set; }
        public PictureDTO Picture { get; set; }

    }
}
