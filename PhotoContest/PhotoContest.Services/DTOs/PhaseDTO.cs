﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Services.DTOs
{
    public class PhaseDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<ContestPhasesDTO> Contests { get; set; }
    }
}
