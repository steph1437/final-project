﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Services.DTOs
{
   public class ContenderContestDTO
    {
        public int Id { get; set; }

        public int ContestId { get; set; }
        public ContestDTO Contest { get; set; }
       

        public int UserId { get; set; }
        public UserDTO User { get; set; }

        public int? PictureId { get; set; }
        public PictureDTO Picture { get; set; }
    }
}
