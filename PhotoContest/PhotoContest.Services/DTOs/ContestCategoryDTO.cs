﻿using PhotoContest.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Services.DTOs
{
    public class ContestCategoryDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<ContestDTO> Contests { get; set; }
    }
}
