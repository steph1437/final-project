﻿using PhotoContest.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Services.DTOs
{
    public class UserDTO
    {
        
        public  int Id { get; set; }

        public string FirstName { get; set; }
     
        public string LastName { get; set; }

        public  string UserName { get ; set ; }

        public string Email { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }

        public Int64 Points { get; set; }

        ICollection<JuryContestDTO> JuryContests { get; set; }

        ICollection<ContenderContestDTO> ContenderContests { get; set; }
    }
}
