﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Services.DTOs
{
    public class PictureDTO
    {
        public int Id { get; set; }


        public string Name { get; set; }


        public string Story { get; set; }


        public string Url { get; set; }

        //public int ContestId { get; set; }
        public ContestDTO Contest { get; set; }

        public ICollection<PictureReviewDTO> PictureReviews { get; set; }

        public ContenderContestDTO ContenderContest { get; set; }

        public bool IsReviewable { get; set; }



    }
}
