﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Services.DTOs
{
    public class JuryContestDTO
    {
        public int Id { get; set; }

        public int UserId { get; set; }
        public UserDTO User { get; set; }

        public int ContestId { get; set; }
        public ContestDTO Contest { get; set; }

        public ICollection<PictureReviewDTO> PictureReviews { get; set; }
    }
}
