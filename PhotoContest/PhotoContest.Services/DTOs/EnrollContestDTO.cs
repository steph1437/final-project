﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Services.DTOs
{
  public  class EnrollContestDTO
    {
        public int UserId { get; set; }

     
        public int ContestId { get; set; }

        public string Name { get; set; }

        public int Id { get; set; }

     
        public string Story { get; set; }


        public string Url { get; set; }

       
   }

}
