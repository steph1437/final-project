﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using PhotoContest.Services.Providers;
using PhotoContest.Services.Providers.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Services.Services
{
    public class ReviewService : IReviewService
    {
        private readonly IMapper mapper;
        private readonly PhotoContestContext dbcontext;
        private readonly IDatеTimeProvider dateTimeProvider;
        public ReviewService(IMapper mapper,
                            PhotoContestContext dbcontext, IDatеTimeProvider dateTimeProvider)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.dbcontext = dbcontext ?? throw new ArgumentNullException(nameof(dbcontext));
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
        }
        private IQueryable<PictureReview> PictureReviewsQuery
        {
            get => dbcontext.PictureReviews
                            .Include(picturereview => picturereview.Picture)
                            .Include(picturereview => picturereview.JuryContest)
                            .Where(picturereview => picturereview.IsDeleted == false);
        }
        public async Task<PictureReviewDTO> CreatePictureReviewAsync(PictureReviewDTO picturereviewDTO)
        {
            ValidatePictureExists(picturereviewDTO.PictureId);
            //ValidateJuriContestExists(picturereviewDTO.JuryContestContestId)  


            var review = mapper.Map<PictureReview>(picturereviewDTO);
            review.CreatedOn = dateTimeProvider.GetDateTime();

            try
            {
                await dbcontext.PictureReviews.AddAsync(review);
                await dbcontext.SaveChangesAsync();
            }
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }

            return mapper.Map<PictureReviewDTO>(review);

        }

        public async Task DeletePictureReviewAsync(int id)
        {
            PictureReview picturereview = TryGetPictureReview(id);

            picturereview.IsDeleted = true;
            picturereview.DeletedOn = this.dateTimeProvider.GetDateTime();

            dbcontext.PictureReviews.Update(picturereview);
            await dbcontext.SaveChangesAsync();
        }

        public async Task<PictureReviewDTO> GetPictureReviewAsync(int id)
        {
            var picturePreview = await this.PictureReviewsQuery
                                           .FirstOrDefaultAsync(picturePreview => picturePreview.Id == id);

            var picturePreviewDTO = mapper.Map<PictureReviewDTO>(picturePreview);

            return picturePreviewDTO;
        }

        public async Task<List<PictureReviewDTO>> GetAllPictureReviewsAsync()
        {
            var pictureReviewDTO = await this.PictureReviewsQuery
                                             .Select(c => mapper.Map<PictureReviewDTO>(c))
                                             .ToListAsync();

            return pictureReviewDTO;
        }

        public async Task UpdatePictureReviewAsync(PictureReviewDTO picturereviewDTO)
        {

            ValidatePictureExists(picturereviewDTO.PictureId);


            PictureReview pictureReview = TryGetPictureReview(picturereviewDTO.Id);

            SetPictureReviewAttributes(pictureReview, picturereviewDTO.Comment);


            //// prevents overating picturereviews's scores with null
               
            pictureReview.ModifiedOn = this.dateTimeProvider.GetDateTime();

            dbcontext.PictureReviews.Update(pictureReview);

            try
            {
               
                await dbcontext.SaveChangesAsync();
            }
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }

        }

        bool ValidatePictureExists(int id)
        {
            var picture = dbcontext.Pictures
                .Where(c => c.IsDeleted == false)
                .FirstOrDefault(c => c.Id == id);

            if (picture == null)
                return false;

            return true;
        }
        private PictureReview TryGetPictureReview(int id)
        {
            var picturereview = dbcontext.PictureReviews
                                         .FirstOrDefault(picrev => picrev.Id == id && picrev.IsDeleted == false);

            if (picturereview == null)
            {
                throw new ResourceNotExistException();
            }
            return picturereview;
        }
        private void SetPictureReviewAttributes(PictureReview pictureToUpdate, string comment)
        {
            if (pictureToUpdate.Comment != comment)
            {
                pictureToUpdate.Comment = comment;
            }
        }

        private bool ValidateJuriContestExists(int id)
        {
            var juryCotestExists = dbcontext.JuryContests.Any(jc => jc.Id == id);

            if (!juryCotestExists)
                throw new ResourseAlreadyExistException("There is no such a jury.");

            return true;
        }

     
    }
}
