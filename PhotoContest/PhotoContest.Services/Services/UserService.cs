﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using PhotoContest.Services.Providers.Contracts;
using Microsoft.AspNetCore.Identity;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Options;
using PhotoContest.Services.Helpers;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

namespace PhotoContest.Services.Services
{
    public class UserService : IUserService
    {
        private readonly IMapper mapper;
        private readonly PhotoContestContext dbcontext;
        private readonly IDatеTimeProvider dateTimeProvider;
      //  private readonly UserManager<User> userManager;
        //private readonly AppSettings appSettings;

        public UserService(IMapper mapper,
                            PhotoContestContext dbcontext,
                            IDatеTimeProvider dateTimeProvider
                           // UserManager<User> userManager,
                           // IOptions<AppSettings> appSettings
            )
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.dbcontext = dbcontext ?? throw new ArgumentNullException(nameof(dbcontext));
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
           // this.userManager = userManager;
           // this.appSettings = appSettings.Value;
        }
        public async Task<UserDTO> GetUserAsync(int id)
        {
            var user = await dbcontext.Users.FirstOrDefaultAsync(user => user.Id == id && user.IsDeleted == false);

            var userDTO = mapper.Map<UserDTO>(user);

            return userDTO;

        }

        public async Task<List<UserDTO>> GetAllUsersAsync()
        {
            var users = dbcontext.Users.Where(user => user.IsDeleted == false);

            return await users.Select(user => mapper.Map<UserDTO>(user)).ToListAsync();
        }


        public async Task<UserDTO> CreateUserAsync(UserDTO userDTO)
        {
            ValidateEmailIsNotTaken(userDTO.Email, userDTO.Id);
            ValidateUserNameIsNotTaken(userDTO.UserName);

            var userEntity = mapper.Map<User>(userDTO);


            try
            {
                await dbcontext.Users.AddAsync(userEntity);
                await dbcontext.SaveChangesAsync();

                var userDTOMapped = mapper.Map<UserDTO>(userEntity);
                return userDTO;
            }
            catch (ValidationException)
            {
                throw new InvalidParameterException("Not a proper input for the database entity columns");
            }
        }

        public async Task UpdateUserAsync(UserDTO userDTO)
        {
            var userEntity = TryGetUser(userDTO.Id);

            ValidateUserNameIsNotTaken(userDTO.UserName);
            ValidateEmailIsNotTaken(userDTO.Email, userDTO.Id);

            SetUserAttributes(userEntity, userDTO.Email, userDTO.UserName);
            userEntity.ModifiedOn = dateTimeProvider.GetDateTime();

            try
            {
                dbcontext.Users.Update(userEntity);
                await dbcontext.SaveChangesAsync();
            }
            catch (ValidationException)
            {
                throw new InvalidParameterException("Not a proper input for the database entity columns");
            }
        }
      /*  public (User, string) Authenticate(string username, string password)
        {
            var user = dbcontext.Users.SingleOrDefault(x => x.Email == username);

            var verifyResult = userManager.PasswordHasher.VerifyHashedPassword(user, user.PasswordHash, password);
            if (verifyResult != PasswordVerificationResult.Success)
            {
                return (null, null);
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("THIS IS USED TO SIGN AND VERIFY JWT TOKENS, REPLACE IT WITH YOUR OWN SECRET, IT CAN BE ANY STRING");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                       new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return (user, tokenHandler.WriteToken(token));
        }*/

        public async Task DeleteUserAsync(int id)
        {

            var user = TryGetUser(id);

            user.IsDeleted = true;


            dbcontext.Users.Update(user);

            try
            {
                await dbcontext.SaveChangesAsync();
            }
            catch (ValidationException e)
            {

                throw new InvalidOperationException(e.Message);
            }
        }

        private bool ValidateEmailIsNotTaken(string email, int id)
        {
            var emailExists = dbcontext.Users.Any(user => user.Email == email && user.Id != id);
            ValidateEmailPattern(email);

            if (emailExists)
            {
                throw new ResourseAlreadyExistException("There is already a user with this Email");
            }

            return true;
        }
        private bool ValidateEmailPattern(string emailString)
        {
            bool isEmailCorrect = Regex.IsMatch(emailString, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);

            if (isEmailCorrect)
            {
                return true;
            }
            else
            {
                throw new InvalidParameterException("Email pattern is NOT CORRECT!");
            }
        }
        private User TryGetUser(int id)
        {
            var user = dbcontext.Users.Where(user => user.IsDeleted == false)
                                      .FirstOrDefault(user => user.Id == id);

            if (user == null)
            {
                throw new ResourceNotExistException();
            }
            else
            {
                return user;
            }
        }

        private bool ValidateUserNameIsNotTaken(string userName)
        {
            var userNameExists = dbcontext.Users.Any(user => user.UserName == userName);

            if (userNameExists)
            {
                throw new ResourseAlreadyExistException("There is already a user with this UserName");
            }

            return true;
        }

        private void SetUserAttributes(User user, string email, string username)
        {
            if (user.UserName != username)
            {
                user.UserName = username;
            }

            if (user.Email != email)
            {
                user.Email = email;
            }
        }

    }
}
