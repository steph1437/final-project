﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhotoContest.Services.Providers.Contracts;


namespace PhotoContest.Services.Services
{
    public class ContestCategoryService : IContestCategoryService
    {
        private readonly IMapper mapper;
        private readonly PhotoContestContext dbcontext;
        private readonly IDatеTimeProvider dateTimeProvider;

        public ContestCategoryService(IMapper mapper,
                            PhotoContestContext dbcontext, IDatеTimeProvider dateTimeProvider)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.dbcontext = dbcontext ?? throw new ArgumentNullException(nameof(dbcontext));
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
        }

        private IQueryable<ContestCategory> ContestCategoriesQuery
        {
            get => this.dbcontext.ContestCategories;
        }
        public async Task<ContestCategoryDTO> CreateContestCategoryAsync(ContestCategoryDTO createData)
        {
            ValidateNameNotTaken(createData.Name);

            var contestCategory = mapper.Map<ContestCategory>(createData);
            contestCategory.CreatedOn = dateTimeProvider.GetDateTime();

            try
            {
                await dbcontext.ContestCategories.AddAsync(contestCategory);
                await dbcontext.SaveChangesAsync();
            }
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }

            return mapper.Map<ContestCategoryDTO>(contestCategory);
        }

        public async Task DeleteContestCategoryAsync(int id)
        {
            var contestCategory = dbcontext.ContestCategories
              .FirstOrDefault(c => c.Id == id)
              ?? throw new ArgumentNullException();

            contestCategory.IsDeleted = true;
            contestCategory.DeletedOn = dateTimeProvider.GetDateTime();


            await dbcontext.SaveChangesAsync();

        }

        public async Task<ContestCategoryDTO> GetContestCategoryAsync(int id)
        {
            var contestCategory = await dbcontext.ContestCategories
                 .FirstOrDefaultAsync(c => c.Id == id && c.IsDeleted == false)
                 ?? throw new ArgumentNullException();

            var contestCategoryDTO = mapper.Map<ContestCategoryDTO>(contestCategory);

            return contestCategoryDTO;
        }
        public async Task<List<ContestCategoryDTO>> GetAllContestCategoriesAsync()
        {
            var contestcategoriesDTO = await this.ContestCategoriesQuery
                                            .Where(a => a.IsDeleted == false)
                                            .Select(c => mapper.Map<ContestCategoryDTO>(c))
                                            .ToListAsync();

            return contestcategoriesDTO;


        }

        public async Task UpdateContestCategoryAsync(ContestCategoryDTO contestCategoryDTO)
        {
            ValidateContestcategoryExist(contestCategoryDTO);

            var contestcategory = TryGetContestcategory(contestCategoryDTO.Id);

            contestcategory.Name = contestCategoryDTO.Name;
            contestcategory.ModifiedOn = this.dateTimeProvider.GetDateTime();

            dbcontext.ContestCategories.Update(contestcategory);

            try
            {
                await dbcontext.SaveChangesAsync();
            }
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }
        }
        private bool ValidateNameNotTaken(string name)
        {
            var nameExists = dbcontext.ContestCategories.Any(c => c.Name == name);

            if (nameExists)
                throw new ResourseAlreadyExistException("There is already a contest category with this name.");

            return true;
        }
        private ContestCategory TryGetContestcategory(int id)
        {
            var category = dbcontext.ContestCategories.AsNoTracking()
                            .Where(beer => beer.IsDeleted == false)
                            .FirstOrDefault(beer => beer.Id == id);

            category = category ?? throw new ResourceNotExistException();
            return category;
        }
        private void ValidateContestcategoryExist(ContestCategoryDTO contestCategoryDTO)
        {
            var categoryExists = dbcontext.ContestCategories.Any(model => model.Name == contestCategoryDTO.Name);

            if (categoryExists)
            {
                throw new ResourseAlreadyExistException("Category with the same name already exists!");
            }
        }
    }
}
