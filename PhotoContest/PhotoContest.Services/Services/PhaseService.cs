﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data.Context;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Services.Services
{
    public class PhaseService : IPhaseService
    {
        private readonly IMapper mapper;
        private readonly PhotoContestContext dbcontext;

        public PhaseService(IMapper mapper, PhotoContestContext dbcontext)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.dbcontext = dbcontext ?? throw new ArgumentNullException(nameof(dbcontext));
        }
        public async Task<List<PhaseDTO>> GetAllContestPhasesAsync()
        {
            var phaseDTO = await this.dbcontext.Phases
                                            .Where(a => a.IsDeleted == false)
                                            .Select(c => mapper.Map<PhaseDTO>(c))
                                            .ToListAsync();

            return phaseDTO;

        }

        public async Task<PhaseDTO> GetContestPhaseAsync(int id)
        {
            var phase = await this.dbcontext.Phases
                                     .Where(domainModel => domainModel.IsDeleted == false)
                                     .FirstOrDefaultAsync(c => c.Id == id && c.IsDeleted == false)
                                     ?? throw new ArgumentNullException();

            var phaseDTO = mapper.Map<PhaseDTO>(phase);

            return phaseDTO;
        }
    }
}
