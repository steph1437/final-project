﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using PhotoContest.Services.Providers.Contracts;


namespace PhotoContest.Services.Services
{
    public class ContestService : IContestService
    {
        private readonly IMapper mapper;
        private readonly PhotoContestContext dbcontext;
        private readonly IDatеTimeProvider datеTimeProvider;

        public ContestService(IMapper mapper,
                            PhotoContestContext dbcontext, IDatеTimeProvider datеTimeProvider)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.dbcontext = dbcontext ?? throw new ArgumentNullException(nameof(dbcontext));
            this.datеTimeProvider = datеTimeProvider ?? throw new ArgumentNullException(nameof(datеTimeProvider));
        }
        public IQueryable<Contest> ContestsQuery
        {
            get => dbcontext.Contests
                           .Include(x => x.ContestCategory)
                           .Include(x => x.ContestState)
                           .Include(x => x.ContestPhases)
                           .Include(x => x.Pictures);

        }
        public ContestPhase GetCurrentPhase(Contest contest)
        {
            DateTime time = DateTime.Now;
            foreach (var phase in dbcontext.Contests.Include(c => c.ContestPhases).FirstOrDefault(c => c.Id == contest.Id).ContestPhases)
            {
                if (time < phase.EndDate && time > phase.StartDate)
                {
                    return phase;
                }
            }

            return null;
        }
        public async Task<List<ContestDTO>> GetAllContestsAsync()
        {
            var contestDTO = await this.dbcontext.Contests
                                            .Include(x => x.ContestCategory)
                                            .Include(x => x.ContestState)
                                            .Include(x => x.ContestPhases)
                                            .Include(x => x.ContenderContests)
                                            .ThenInclude(x=>x.User)
                                            .Where(a => a.IsDeleted == false)
                                            .Select(c => mapper.Map<ContestDTO>(c))
                                            .ToListAsync();

            return contestDTO;


        }

        public async Task<ICollection<ContestDTO>> ListAllFilterHybrid(string searchCriteria)
        {
            var query = this.ContestsQuery;



            query = Search(query, searchCriteria);

            return await query.Select(contest => mapper.Map<ContestDTO>(contest)).ToListAsync();
        }

        public IQueryable<Contest> Search(IQueryable<Contest> query, string keyword)
        {
            if (keyword != null)
            {
                query = query.Where(contest => contest.ContestCategoryPhaseDescription.Contains(keyword));
            }
            return query;
        }

        public async Task<ContestDTO> CreateContestAsync(ContestDTO createData)
        {
            ValidateTitleDoesNotExist(createData.Title);
            ValidateContestStateExists(createData.ContestStateId);
            ValidateContestCategoryExists(createData.ContestCategoryId);



            var contest = mapper.Map<Contest>(createData);
            contest.CreatedOn = datеTimeProvider.GetDateTime();

            try
            {
                await dbcontext.Contests.AddAsync(contest);
                await dbcontext.SaveChangesAsync();
            }
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }

            return mapper.Map<ContestDTO>(contest);
        }

        public async Task<ContestOutputFrontDTO> CreateAsync(ContestInputDTO createData)
        {
            if (createData == null)
            {
                throw new ArgumentNullException("Input cant be null");
            }

            createData.Phases.PhaseOne_StartDate = DateTime.UtcNow;
            createData.Phases.PhaseTwo_StartDate = createData.Phases.PhaseOne_EndDate;
            createData.Phases.PhaseFinish_StartDate = createData.Phases.PhaseTwo_EndDate;
            createData.Phases.Phasefinish_EndDate = DateTime.MaxValue;

            var contest = mapper.Map<Contest>(createData);

            await dbcontext.Contests.AddAsync(contest);

            try
            {
                await this.dbcontext.SaveChangesAsync();
            }
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }

            await this.AddContestToPhasesAsync(createData, contest.Id);

            try
            {
                await this.dbcontext.SaveChangesAsync();
            }
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }

            return mapper.Map<ContestOutputFrontDTO>(contest);

        }

        public async Task DeleteContestAsync(int id)
        {
            Contest contest = TryGetContest(id);

            contest.IsDeleted = true;
            contest.DeletedOn = datеTimeProvider.GetDateTime();


            dbcontext.Update(contest);
            await dbcontext.SaveChangesAsync();

        }

        public async Task<ContestDTO> GetContestAsync(int id)
        {
            var contest = await this.ContestsQuery
                                    .FirstOrDefaultAsync(c => c.Id == id && c.IsDeleted == false)
                                    ?? throw new ArgumentNullException();
            var currentPhase = GetCurrentPhase(contest);
            var contestDTO = mapper.Map<ContestDTO>(contest);
            if (currentPhase.PhaseId == 1)
            {
                contestDTO.ContestCategoryPhaseDescription = "Phase One";
            }
            else if (currentPhase.PhaseId == 2)
            {
                contestDTO.ContestCategoryPhaseDescription = "Phase Two";
            }
            else
            {
                contestDTO.ContestCategoryPhaseDescription = "Finished";
            }
            contestDTO.Phase = currentPhase;
            return contestDTO;
        }
        public async Task<List<UserViewDTO>> SeeContestUsers(int id)
        {
            var users = await this.dbcontext.ContenderContests
                .Include(x => x.Contest)
                .Include(x => x.User)
                .Where(x => x.ContestId == id)
                .Select(x => x.User)
                .ToListAsync();

            List<UserViewDTO> contestUsers = new List<UserViewDTO>();
            foreach(var user in users)
            {
                contestUsers.Add(mapper.Map<UserViewDTO>(user));
            }

            return contestUsers;
        }
        public async Task UpdateContestAsync(ContestDTO updateData)
        {
            ValidateTitleDoesNotExist(updateData.Title);
            ValidateContestStateExists(updateData.ContestStateId);

            ValidateContestCategoryExists(updateData.ContestCategoryId);
            var contest = TryGetContest(updateData.Id);

            SetContestAttributes(contest, updateData.Title, updateData.ContestStateId, updateData.ContestCategoryPhaseDescription, updateData.ContestCategoryId);
            contest.ModifiedOn = datеTimeProvider.GetDateTime();

            try
            {
                dbcontext.Contests.Update(contest);
                await dbcontext.SaveChangesAsync();
            }
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }
        }

        private Contest TryGetContest(int id)
        {
            var contest = dbcontext.Contests
                .Where(c => c.IsDeleted == false)
                .FirstOrDefault(c => c.Id == id)
                ?? throw new ResourceNotExistException();

            return contest;
        }

        private bool ValidateTitleDoesNotExist(string title)
        {
            var titleExists = dbcontext.Contests.Any(c => c.Title == title);

            if (titleExists)
                throw new ResourseAlreadyExistException("There is already a contest with this title.");

            return true;
        }
        private bool ValidateContestCategoryExists(int id)
        {
            var contestCategoryExists = dbcontext.ContestCategories.Any(cc => cc.Id == id);

            if (!contestCategoryExists)
                throw new ResourseAlreadyExistException("There is no such category.");

            return true;
        }
        private bool ValidateContestStateExists(int id)
        {
            var contestStateExists = dbcontext.ContestStates.Any(cs => cs.Id == id);

            if (!contestStateExists)
                throw new ResourseAlreadyExistException("There is no such category state.");

            return true;
        }

        private void SetContestAttributes(Contest contestToUpdate, string title,
            int contestStateId, string contestCategoryPhaseDescription, int contestCategoryId)
        {
            if (contestToUpdate.Title != title)
            {
                contestToUpdate.Title = title;
            }
            if (contestToUpdate.ContestCategoryPhaseDescription != contestCategoryPhaseDescription)
            {
                contestToUpdate.ContestCategoryPhaseDescription = contestCategoryPhaseDescription;
            }

            if (contestToUpdate.ContestStateId != contestStateId)
            {
                contestToUpdate.ContestStateId = contestStateId;
                contestToUpdate.ContestState = dbcontext.ContestStates
                    .FirstOrDefault(cs => cs.Id == contestStateId)
                    ?? throw new ResourceNotExistException();
            }
            if (contestToUpdate.ContestCategoryId != contestCategoryId)
            {
                contestToUpdate.ContestCategoryId = contestCategoryId;
                contestToUpdate.ContestCategory = dbcontext.ContestCategories
                    .FirstOrDefault(cs => cs.Id == contestCategoryId)
                    ?? throw new ResourceNotExistException();
            }


        }
        private async Task AddContestToPhasesAsync(ContestInputDTO model, int id)
        {
            await this.dbcontext.ContestPhases
                .AddAsync(new ContestPhase()
                {
                    ContestId = id,
                    PhaseId = 1,
                    StartDate = model.Phases.PhaseOne_StartDate,
                    EndDate = model.Phases.PhaseOne_EndDate,
                }); ;

            await this.dbcontext.ContestPhases
                .AddAsync(new ContestPhase()
                {
                    ContestId = id,
                    PhaseId = 2,
                    StartDate = model.Phases.PhaseTwo_StartDate,
                    EndDate = model.Phases.PhaseTwo_EndDate,
                });

            await this.dbcontext.ContestPhases
              .AddAsync(new ContestPhase()
              {
                  ContestId = id,
                  PhaseId = 3,
                  StartDate = model.Phases.PhaseFinish_StartDate,
                  EndDate = model.Phases.Phasefinish_EndDate,
              });
        }
    }
}
