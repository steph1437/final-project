﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Data.Models;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Services.Services
{
    public class JunkieService : IJunkieService
    {
        private readonly PhotoContestContext dbcontext;
        private readonly UserManager<User> userManager;
        private readonly IMapper mapper;
        public JunkieService(PhotoContestContext dbcontext, UserManager<User> userManager, IMapper mapper)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.dbcontext = dbcontext ?? throw new ArgumentNullException(nameof(dbcontext));
            this.userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }
        public async Task<bool> IsUserJury(int contestId, int userId)
        {
            if (contestId <= 0)
            {
                throw new InvalidParameterException(string.Format("Invalid contest Id"));
            }

            if (userId <= 0)
            {
                throw new InvalidParameterException(string.Format("Invalid user Id"));
            }

            var isJury = await this.dbcontext.JuryContests
                .AnyAsync(p => p.JuryContestUserId == userId && p.JuryContestContestId == contestId);

            return isJury;
        }
        public async Task EnrollForContestAsync(EnrollContestDTO inputModel)
        {
            if (inputModel == null)
            {
                throw new ArgumentNullException(string.Format("The input cant be null."));
            }

            var participant = await this.dbcontext.ContenderContests.FirstOrDefaultAsync(pc => pc.UserId == inputModel.UserId &&
                    pc.ContestId == inputModel.ContestId);

           // if currently participating without photo
            //if (participant != null)
            //{
                var picture = mapper.Map<Picture>(inputModel);

                var addedpicture = await this.dbcontext.Pictures.AddAsync(picture);

                await this.dbcontext.SaveChangesAsync();

            //}
            //else
            //{
            //    var toAddParticipantContest = new ContenderContest()
            //    {

            //        ContestId = inputModel.ContestId,
            //        UserId = inputModel.UserId,
            //        IsDeleted = false,
            //        CreatedOn = DateTime.UtcNow,
            //        Picture = new Picture()
            //        {
            //            Name = inputModel.PictureName,
            //            Url = inputModel.PictureUrl,
            //            IsDeleted = false,
            //            Story = inputModel.PhotoDescription,
            //            CreatedOn = DateTime.UtcNow,
            //            ContestId = inputModel.ContestId,
            //        }
            //    };

            //    await this.dbcontext.ContenderContests.AddAsync(toAddParticipantContest);



            //await this.dbcontext.SaveChangesAsync();
        }


    }
}
