﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data.Context;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Services.Services
{
    public class ContestStateService : IContestStateService
    {
        private readonly IMapper mapper;
        private readonly PhotoContestContext dbcontext;

        public ContestStateService(IMapper mapper, PhotoContestContext dbcontext)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.dbcontext = dbcontext ?? throw new ArgumentNullException(nameof(dbcontext));
        }
        public async Task<List<ContestStateDTO>> GetAllContestStatesAsync()
        {
                var stateDTO = await this.dbcontext.ContestStates
                                                   .Where(a => a.IsDeleted == false)
                                                   .Select(c => mapper.Map<ContestStateDTO>(c))
                                                   .ToListAsync();

            return stateDTO;

        }

        public async Task<ContestStateDTO> GetContestStateAsync(int id)
        {
            var state = await this.dbcontext .ContestStates
                                             .Where(domainModel => domainModel.IsDeleted == false)
                                             .FirstOrDefaultAsync(c => c.Id == id && c.IsDeleted == false)
                                             ?? throw new ArgumentNullException();

            var stateDTO = mapper.Map<ContestStateDTO>(state);

            return stateDTO;
        }
    }
}
