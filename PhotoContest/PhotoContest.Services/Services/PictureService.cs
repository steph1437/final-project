﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data;
using PhotoContest.Data.Context;
using PhotoContest.Services.Contracts;
using PhotoContest.Services.DTOs;
using PhotoContest.Services.Exceptions;
using PhotoContest.Services.Providers.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Services.Services
{
    public class PictureService : IPictureService
    {
        private readonly IMapper mapper;
        private readonly PhotoContestContext dbcontext;
        private readonly IDatеTimeProvider dateTimeProvider;
        public PictureService(IMapper mapper,
                            PhotoContestContext dbcontext, IDatеTimeProvider dateTimeProvider)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.dbcontext = dbcontext ?? throw new ArgumentNullException(nameof(dbcontext));
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
        }
        private IQueryable<Picture> PicturesQuery
        {
            get => this.dbcontext.Pictures
                                 .Include(x=>x.PictureReviews)
                                 .Include(x => x.Contest);
        }
        public async Task<PictureDTO> CreatePictureAsync(PictureDTO createData)
        {
           
            ValidateContestExists(createData.Contest.Id);
            ValidatePictureNameNotExists(createData.Name, createData.Id);

            var picture = mapper.Map<Picture>(createData);
            picture.CreatedOn = this.dateTimeProvider.GetDateTime();

            try
            {
                await dbcontext.Pictures.AddAsync(picture);
                await dbcontext.SaveChangesAsync();
            }
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }

            var pictureDTO = mapper.Map<PictureDTO>(picture);
            return pictureDTO;
        }

        public async Task DeletePictureAsync(int id)
        {
            Picture picture = TryGetPicture(id);

            picture.IsDeleted = true;
            picture.DeletedOn = this.dateTimeProvider.GetDateTime();

            dbcontext.Pictures.Update(picture);

            await dbcontext.SaveChangesAsync();

        }

        public async Task<List<PictureDTO>> GetAllPicturesAsync()
        {
            var pictureDTO = await this .PicturesQuery
                                        .Where(a => a.IsDeleted == false)
                                        .Select(c => mapper.Map<PictureDTO>(c))
                                        .ToListAsync();

            return pictureDTO;
        }

        public async Task<PictureDTO> GetPictureAsync(int id)
        {
            var contest = await this.dbcontext.Pictures
                                              .Include(x=>x.Contest).ThenInclude(x=>x.ContestCategory)
                                              .Include(x=>x.PictureReviews)
                                             
                                              
                                    .FirstOrDefaultAsync(c => c.Id == id && c.IsDeleted == false)
                                    ?? throw new ArgumentNullException();

            var pictureDTO = mapper.Map<PictureDTO>(contest);

            return pictureDTO;
        }

        public async Task UpdatePictureAsync(PictureDTO updateData)
        {
            ValidateContestExists(updateData.Contest.Id);
            

            Picture picture = TryGetPicture(updateData.Id);

            if (picture.Name != updateData.Name)
            {
                ValidatePictureNameNotExists(updateData.Name, updateData.Id);
            }
            
            SetPictureAttributes(picture, updateData.Name, updateData.Story,updateData.Contest.Id);     
           
            picture.ModifiedOn = this.dateTimeProvider.GetDateTime();

            try
            {
                dbcontext.Pictures.Update(picture);
                await dbcontext.SaveChangesAsync();
            }
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }
        }

        private Picture TryGetPicture(int id)
        {
            var picture = dbcontext.Pictures
                .Where(p => p.IsDeleted == false)
                .FirstOrDefault(p => p.Id == id)
                ?? throw new ResourceNotExistException();

            return picture;
        }

        bool ValidateContestExists(int id)
        {
            var contest = dbcontext.Contests
                .Where(c => c.IsDeleted == false)
                .FirstOrDefault(c => c.Id == id);

            if (contest == null)
                return false;

            return true;
        }


        private void ValidatePictureNameNotExists(string pictureName, int pictureId)
        {
            bool pictureNameExists = dbcontext.Pictures.Any(picture => (
                picture.Name == pictureName &&
                picture.Id == pictureId &&
                picture.IsDeleted == false)
            );

            if (pictureNameExists)
            {
                throw new ResourseAlreadyExistException();
            }
        }
        private void SetPictureAttributes(Picture pictureToUpdate, string name,
              string story, int contestId)
        {
            if (pictureToUpdate.Name != name)
            {
                pictureToUpdate.Name = name;
            }
            if (pictureToUpdate.Story != story)
            {
                pictureToUpdate.Story = story;
            }

            if (pictureToUpdate.ContestId != contestId)
            {
                pictureToUpdate.ContestId = contestId;
                pictureToUpdate.Contest = dbcontext.Contests
                    .FirstOrDefault(cs => cs.Id == contestId)
                    ?? throw new ResourceNotExistException();
            }
            
        }

        public async Task< ICollection<PictureDTO>> GetTopThreeRatedPictureAsync()
        {
            var query = await this.dbcontext.Pictures
                              .OrderByDescending(picture => picture.ContenderContest.CreatedOn)
                              .OrderByDescending(picture => picture.PictureReviews.Sum(picture=>picture.Score)/picture.PictureReviews.Count())
                              .Take(3)
                              .AsNoTracking()
                              .ToListAsync();

            return query.Select(beer => mapper.Map<PictureDTO>(beer)).ToList();
        }

    }
}
