﻿using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IPhaseService
    {
        Task<PhaseDTO> GetContestPhaseAsync(int id);
        Task<List<PhaseDTO>> GetAllContestPhasesAsync();
    }
}
