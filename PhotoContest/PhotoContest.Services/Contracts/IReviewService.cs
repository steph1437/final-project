﻿using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IReviewService
    {
        Task<PictureReviewDTO> GetPictureReviewAsync(int id);

        Task<List<PictureReviewDTO>> GetAllPictureReviewsAsync();

        Task<PictureReviewDTO> CreatePictureReviewAsync(PictureReviewDTO userDTO);

        Task UpdatePictureReviewAsync(PictureReviewDTO pictureReviewDTO);

        Task DeletePictureReviewAsync(int id);

    }
}
