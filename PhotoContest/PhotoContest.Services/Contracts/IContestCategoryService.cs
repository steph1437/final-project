﻿using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IContestCategoryService
    {
        Task UpdateContestCategoryAsync(ContestCategoryDTO contestCategoryDTO);

        Task<List<ContestCategoryDTO>> GetAllContestCategoriesAsync();

        Task<ContestCategoryDTO> GetContestCategoryAsync(int id);

        Task<ContestCategoryDTO> CreateContestCategoryAsync(ContestCategoryDTO createData);

        Task DeleteContestCategoryAsync(int id);
    }
}
