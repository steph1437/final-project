﻿using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IContestService
    {
        Task<ContestDTO> GetContestAsync(int id);
        Task<List<ContestDTO>> GetAllContestsAsync();
        Task<ContestDTO> CreateContestAsync(ContestDTO createData);
        Task DeleteContestAsync(int id);
        Task UpdateContestAsync(ContestDTO updateData);
        Task<ICollection<ContestDTO>> ListAllFilterHybrid(string searchCriteria);
        Task<ContestOutputFrontDTO> CreateAsync(ContestInputDTO createData);
        Task<List<UserViewDTO>> SeeContestUsers(int id);
    }
}
