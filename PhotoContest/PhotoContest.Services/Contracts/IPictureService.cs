﻿using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IPictureService
    {
        //ICollection<PictureDTO> GetTopThreeRatedPicture();
        Task<ICollection<PictureDTO>> GetTopThreeRatedPictureAsync();
        Task< PictureDTO> GetPictureAsync(int id);
        Task<List<PictureDTO>> GetAllPicturesAsync();
        Task<PictureDTO> CreatePictureAsync(PictureDTO createData);
        Task UpdatePictureAsync(PictureDTO updateData);
        Task DeletePictureAsync(int id);
    }
}
