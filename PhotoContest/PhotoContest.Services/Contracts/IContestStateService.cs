﻿using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IContestStateService
    {
        Task<ContestStateDTO> GetContestStateAsync(int id);
        Task<List<ContestStateDTO>> GetAllContestStatesAsync();
    }
}
