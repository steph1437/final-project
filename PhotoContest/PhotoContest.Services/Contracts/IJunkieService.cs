﻿using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Services.Contracts
{
    public interface IJunkieService
    {
        Task<bool> IsUserJury(int contestId, int userId);
        Task EnrollForContestAsync(EnrollContestDTO inputModel);
    }
}
