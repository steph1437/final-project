﻿using AutoMapper;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContest.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Services.DTOMappers
{
    public class DTOAutoMapper:Profile
    {
        public DTOAutoMapper()
        {
            CreateMap<User, UserDTO>().ReverseMap();
            CreateMap<Contest, ContestDTO>().ReverseMap();
            CreateMap<Contest, ContestInputDTO>().ReverseMap();
            CreateMap<Contest, ContestOutputFrontDTO>().ReverseMap();
            CreateMap<ContestInputDTO, ContestOutputFrontDTO>().ReverseMap();
            CreateMap<ContestCategory, ContestCategoryDTO>().ReverseMap();
            CreateMap<PictureReview, PictureReviewDTO>().ReverseMap();
            CreateMap<Phase, PhaseDTO>().ReverseMap();
            CreateMap<ContestPhase, ContestPhasesDTO>().ReverseMap();
            CreateMap<ContestState, ContestStateDTO>().ReverseMap();
            CreateMap<Picture, PictureDTO>().ReverseMap();
            CreateMap<User, UserViewDTO>().ReverseMap();
            CreateMap<Picture, EnrollContestDTO>().ReverseMap();

        }
    }
}
