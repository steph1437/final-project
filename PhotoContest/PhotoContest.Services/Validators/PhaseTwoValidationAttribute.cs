﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoContest.Services.Validators
{
    public class PhaseTwoValidationAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var valueAsInteger = (int)value;

            if (valueAsInteger < 1 || valueAsInteger > 24)
            {
                return false;
            }

            return true;
        }
    }
}
