﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoContest.Services.Validators
{
    public class ContestsDatesValidatorAttribute : ValidationAttribute
    {
        public string DisplayPropertyName { get; set; }
        public string MoreThanPropertyName { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var phase = (DateTime)value;

            var containerType = validationContext.ObjectInstance.GetType();

            if (phase < DateTime.UtcNow)
            {
                return new ValidationResult(ErrorMessage = "Date cannot be in the past");
            }

            if (MoreThanPropertyName != null)
            {
                var field = containerType.GetProperty(MoreThanPropertyName);
                if (field != null)
                {
                    var phaseII = (DateTime)field.GetValue(validationContext.ObjectInstance, null);
                    if (phaseII > phase)
                    {
                        if (DisplayPropertyName == null)
                        {
                            throw new Exception();
                        }
                        return new ValidationResult(ErrorMessage = $"Date cannot be before { DisplayPropertyName}");
                    }
                }
            }

            return ValidationResult.Success;
        }
    }
}
