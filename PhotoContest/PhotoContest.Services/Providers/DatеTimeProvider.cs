﻿using PhotoContest.Services.Providers.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Services.Providers
{
    public class DatеTimeProvider : IDatеTimeProvider
    {
        public DateTime GetDateTime() => DateTime.UtcNow;
    }
}
