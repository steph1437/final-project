﻿using PhotoContest.Services.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoContest.Services.Providers
{
    public class PhaseProvider
    {
        public DateTime PhaseOne_StartDate { get; set; }

        [Display(Name = "Phase One End Date")]
        [ContestsDatesValidator]
        public DateTime PhaseOne_EndDate { get; set; }

        public DateTime PhaseTwo_StartDate { get; set; }

        [Required]
        [Display(Name = "Phase Two Duration")]
        [PhaseTwoValidation]
        public int PhaseTwo_Duration { get; set; }
        public DateTime PhaseTwo_EndDate
        {
            get
            {
                return PhaseOne_EndDate
                    .AddHours(PhaseTwo_Duration);
            }
        }

        public DateTime PhaseFinish_StartDate { get; set; }
        public DateTime Phasefinish_EndDate{ get; set; }
    }
}
